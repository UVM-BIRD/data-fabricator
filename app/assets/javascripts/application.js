/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui/datepicker
//= require turbolinks
//= require_tree .

/////////////////////////////////////////////////////////////////////////////////
// Hover logic
//

function hoverOn(element) {
    $(element).addClass('hover');
}

function hoverOff(element) {
    $(element).removeClass('hover');
}

function enableHover(selector) {
    $(document).on({
        mouseover: function() { hoverOn($(this)); },
        mouseleave: function() { hoverOff($(this)); }
    }, selector);
}

////////////////////////////////////


function copy(obj) {
    return $(obj).clone(true).get(0);
}

function isDomainAttrRef(value) {
    if (value !== undefined && value.match(/^(?:[a-z]\w*\.)?[a-z]\w*$/)) {
        var el = $('#valid_domain_attr_info');
        if (el.length) {
            var list = $.parseJSON(el.text());

            var valid = false;
            $(list).each(function() {
                if (this == value) {
                    valid = true;
                    return false;
                }
            });
            return valid;

        } else {
            return false;
        }

    } else {
        return false;
    }
}

function isBoolean(value) {
    return value == 'true' || value == 'false';
}


// retrieve the operands from a mathematical expression, at the same time validating the expression's form
// returns an array of operands, or null if the expression doesn't adhere to proper syntax
function getExpressionParts(s) {
    var parts = [];
    var currentPart = '';
    var lmc = null;             // last meaningful character

    for (var i = 0; i < s.length; i ++) {
        var c = s.charAt(i);
        if (c == '(') {
            if (lmc != null && lmc != '^' && lmc != '*' && lmc != '/' && lmc != '+' && lmc != '-') {
                return null;
            }

            var start = i + 1;
            var end = s.lastIndexOf(')');

            if (start >= 0 && end > start) {
                var subParts = getExpressionParts(s.substring(start, end));
                if (subParts == null) {
                    return null;
                }

                for (var j = 0; j < subParts.length; j ++) {
                    parts.push(subParts[j]);
                }

                i = end + 1;
                lmc = ')';

            } else {
                return null;
            }

        } else {
            if (c == '^' || c == '*' || c == '/' || c == '+' || c == '-') {
                if (currentPart.trim() != '') {
                    parts.push(currentPart.trim());
                    currentPart = '';

                } else if (lmc != ')') {
                    return null;
                }

            } else {
                if (lmc == ')' && c != ' ') {
                    return null;

                } else {
                    currentPart += c;
                }
            }

            if (c != ' ') {
                lmc = c;
            }
        }
    }

    // cannot end with an operator
    if (lmc == '^' || lmc == '*' || lmc == '/' || lmc == '+' || lmc == '-') {
        return null;
    }

    if (currentPart.trim() != '') {
        parts.push(currentPart.trim());
    }

    return parts;
}

function isIntegerOrExpression(value) {
    var parts = getExpressionParts(value);

    if (parts == null || parts.length == 0) {
        return false;

    } else {
        var allPartsValid = true;

        for (var i = 0; i < parts.length; i ++) {
            if ( ! isIntegerOrRef(parts[i]) ) {
                allPartsValid = false;
                return false;
            }
        }

        return allPartsValid;
    }
}

function isIntegerOrRef(value) {
    return isInteger(value) || isDomainAttrRef(value);
}

function isInteger(value) {
    if (value !== undefined && value.match(/^\d+$/)) {
        var i = parseInt(value);
        return !isNaN(i) && i >= 0;

    } else {
        return false;
    }
}

function isExpression(value) {
    var regex = /^expr\{(.*)\}$/;
    var match = regex.exec(value);
    if (match != null) {
        var parts = getExpressionParts(match[1]);

        if (parts == null || parts.length == 0) {
            return false;

        } else {
            var allPartsValid = true;

            for (var i = 0; i < parts.length; i ++) {
                if ( ! isDecimal(parts[i]) && ! isDomainAttrRef(parts[i]) ) {
                    allPartsValid = false;
                    return false;
                }
            }

            return allPartsValid;
        }

    } else {
        return false;
    }
}

function isDecimalOrRef(value) {
    return isDecimal(value) || isDomainAttrRef(value);
}

function isDecimal(value) {
    if (value !== undefined && value.match(/^\d+(\.\d+)?$/)) {
        var i = parseFloat(value);
        return ! isNaN(i) && i >= 0;

    } else {
        return false;
    }
}

function isDateOrRef(value) {
    return isDate(value) || isDomainAttrRef(value);
}

function isDate(value) {
    if (value !== undefined && value.match(/^\d{1,2}\/\d{1,2}\/\d{4}$/)) {
        return ! isNaN(Date.parse(value));

    } else {
        return false;
    }
}

function isEnum(value) {
    return value !== undefined && value.match(/^\w((\\,|[^,])*\w)?$/);
}

function isStringTemplate(value) {
    // note : this regex must be kept in sync with Attr::REGEX_STR_TEMPLATE

    var valid = value.match(/^(\\[aA0u\\,{}]|\{(?:[a-z]\w*\.)?[a-z]\w*\}|[^\\,{}])+$/);

    if (valid) {
        var regex = /\{((?:[a-z]\w*\.)?[a-z]\w*)\}/g;
        var match = regex.exec(value);
        while (match != null) {
            if ( ! isDomainAttrRef(match[1]) ) {
                valid = false;
                break;
            }
            match = regex.exec(value);
        }
    }

    return valid;
}

function compare(a, b, dataType) {
    if (a == '' || b == '') return null;

    var A;
    var B;
    if (dataType == 'int' && isIntegerOrRef(a) && isIntegerOrRef(b)) {
        A = parseInt(a);
        B = parseInt(b);

    } else if (dataType == 'decimal' && isDecimalOrRef(a) && isDecimalOrRef(b)) {
        A = parseFloat(a);
        B = parseFloat(b);

    } else if ((dataType == 'Date' || dataType == 'DateTime') && isDateOrRef(a) && isDateOrRef(b)) {
        A = Date.parse(a);
        B = Date.parse(b);
    }

    if (A !== undefined && B !== undefined) {
        if      (A < B)     return -1;
        else if (A == B)    return 0;
        else if (A > B)     return 1;
    }

    return null;
}

function isValidValueForDataType(value, dataType) {
    var v;
    var cls;
    if ($.type(value) == 'object') {
        v = $(value).val();
        cls = $(value).attr('class') !== undefined ?
            $(value).attr('class').split(/\s+/) :
            null;

    } else {
        v = value;
        cls = null;
    }

    if (dataType == 'boolean') {
        return isBoolean(v) || isDomainAttrRef(v);

    } else if (dataType == 'int') {
        if      ($.inArray('lit', cls) >= 0)    return isInteger(v);
        else if ($.inArray('ref', cls) >= 0)    return isDomainAttrRef(v);
        else if ($.inArray('expr', cls) >= 0)   return isExpression('expr{' + v + '}');
        else                                    return isExpression(v) || isDomainAttrRef(v) || isInteger(v);

    } else if (dataType == 'decimal') {
        if      ($.inArray('lit', cls) >= 0)    return isDecimal(v);
        else if ($.inArray('ref', cls) >= 0)    return isDomainAttrRef(v);
        else if ($.inArray('expr', cls) >= 0)   return isExpression('expr{' + v + '}');
        else                                    return isExpression(v) || isDomainAttrRef(v) || isDecimal(v);

    } else if (dataType == 'Date' || dataType == 'DateTime') {
        if      ($.inArray('lit', cls) >= 0)    return isDate(v);
        else if ($.inArray('ref', cls) >= 0)    return isDomainAttrRef(v);
        else                                    return isDate(v) || isDomainAttrRef(v);

    } else if (dataType == 'Enum') {
        return isEnum(v);

    } else if (dataType == 'StringTemplate') {
        return isStringTemplate(v);

    } else {
        return false;
    }
}

function validateString(input, regex) {
    if ($(input).length == 0) return;

    var val = $(input).val();
    var valid = val.match(regex);
    var submit = $(input).closest('form').children('input[type="submit"]').first();

    if (valid) {
        $(input).removeClass('invalid');
        $(submit).removeAttr('disabled');

    } else {
        if (val.match(/^\s*$/)) $(input).removeClass('invalid');
        else                    $(input).addClass('invalid');
        $(submit).attr('disabled', true);
    }
}

function isFormValid(form) {
    var valid = true;
    $(form).find('input, select').each(function() {
        if ($(this).hasClass('invalid')) {
            valid = false;
            return false;
        }
    });
    return valid;
}

$(document).on('change', 'form.auto_submit input, form.auto_submit select', function() {
    var form = $(this).closest('form');
    if ( ! $(this).val().match(/^\s*$/) && isFormValid(form) ) {
        $(form).submit();
    }
});