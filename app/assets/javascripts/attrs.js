/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function getDataType() {
    return $('#this_attribute').attr('data-type');
}

function doAttributeValueFormSetup() {
    hideAlternateInputs();

    $('#domainBuilder').find('.toggle').each(function() {
        setInputToggleText(this);
    });

    $('#new_attr_val_form').find('input.date').each(function() {
        $(this).datepicker({
            showOn: 'button',
            buttonImageOnly: true,
            buttonText: 'Calendar',

            onSelect: function (date) {
                validateDomainParts();
            }
        }).next('img').addClass('ui-icon ui-icon-calendar');
    });

    adjustDomainBuilderFormElements();
    resetNewAttrValForm();
}

function adjustDomainBuilderFormElements() {
    var domain = $('#domain');
    if (domain.length) {
        var addButton = $('#add_to_domain');
        if (addButton.length) {
            var top = Math.round($(addButton).offset().top);
            var left = Math.round($(addButton).offset().left + $(addButton).width()) + 30;

            $(domain).css({ top: top, left: left, position: 'absolute' });
        }
    }
}

function resetNewAttrValForm() {
    var firstDomainPartInput;
    var form = $('#new_attr_val_form');

    if ($('#domainBuilder').length) {
        $(form).find('input.domainPart').each(function() {
            if (firstDomainPartInput === undefined) {
                firstDomainPartInput = this;
            }
            setValue(this, '');
            $(this).removeClass('invalid');
        });

        validateDomainParts();

    } else {
        var input = $(form).find('input:visible').first();
        if ($(input).length) {
            firstDomainPartInput = input;
        }
    }

    var domain = $('#attr_val_domain');
    setValue(domain, '');
    $(domain).removeAttr('invalid');

    $('#new_attr_val_submit').attr('disabled', true);

    if (firstDomainPartInput !== undefined) {
        $(firstDomainPartInput).focus();
    }
}

function anyDomainPartsFlaggedInvalid() {
    return $('#new_attr_val_form').find('.domainPart.invalid:visible').length > 0;
}

function allDomainPartsEmpty() {
    var allEmpty = true;
    $('#new_attr_val_form').find('.domainPart:visible').each(function() {
        if ( ! $(this).val().match(/^\s*$/) ) {
            allEmpty = false;
            return false;
        }
    });
    return allEmpty;
}

function validateDomainParts() {
    var domainBuilder = $('#domainBuilder');

    if (allDomainPartsEmpty()) {
        $(domainBuilder).find('.domainPart.invalid:visible').each(function () {
            $(this).removeClass('invalid');
        });
        $('#add_to_domain').attr('disabled', true);

    } else {
        var dataType = getDataType();
        var req = $(domainBuilder).find('.domainPart:not(.optional):visible').first();

        if (isValidValueForDataType(req, dataType)) req.removeClass('invalid');
        else                                        req.addClass('invalid');

        var opt = $(domainBuilder).find('.domainPart.optional:visible').first();
        if (opt.length) {
            if (opt.val().match(/^\s*$/)) {
                opt.removeClass('invalid');

            } else if (isValidValueForDataType(opt, dataType)) {
                opt.removeClass('invalid');

                if ( ! req.hasClass('invalid') && ! isDomainAttrRef(req.val()) && ! isDomainAttrRef(opt.val()) ) {
                    if (compare(req.val(), opt.val(), dataType) > 0) {
                        opt.addClass('invalid');
                    }
                }

            } else {
                opt.addClass('invalid');
            }
        }

        if (anyDomainPartsFlaggedInvalid()) {
            $('#add_to_domain').attr('disabled', true);

        } else {
            $('#add_to_domain').removeAttr('disabled');
        }
    }
}

String.prototype.reverse = function() {
    return this.split('').reverse().join('');
};

function getDomainRangeParts(value) {
    var regex = /^(expr\{[^}]+\}|[^-]+)\s*(?:-\s*(expr\{[^}]+\}|[^-]+))?$/;
    var match = value.match(regex);

    var arr = [];
    if (match != null) {
        arr.push(match[1].trim());
        if (match[2] !== undefined) arr.push(match[2].trim());
    }
    return arr;
}

function isDomainRange(value) {
    return getDomainRangeParts(value).length == 2;
}

function validateDomain() {
    var domain = $('#attr_val_domain');
    var dataType = getDataType();
    var val = $(domain).val();
    var valid = true;

    if ($('#domainBuilder').length) {
        var parts;
        if (dataType == 'StringTemplate' || dataType == 'Enum') {
            // the following logic is a workaround to javascript's inability to perform negative
            // lookbehinds

            parts = [];
            var arr = val.reverse().split(/\s*,(?!\\)\s*/);
            $.each(arr.reverse(), function() {
                parts.push(this.reverse());
            });

        } else {
            parts = val.split(/\s*,\s*/);
        }

        for (var i = 0; i < parts.length; i ++) {
            if (parts[i] == 'null') {
                valid = true;

            } else if (dataType !== 'StringTemplate' && isDomainRange(parts[i])) {
                var subParts = getDomainRangeParts(parts[i]);

                valid = isValidValueForDataType(subParts[0], dataType) &&
                        isValidValueForDataType(subParts[1], dataType);

                if ( valid && ! isDomainAttrRef(subParts[0]) && ! isDomainAttrRef(subParts[1])
                           && ! isExpression(subParts[0])    && ! isExpression(subParts[1])) {
                    valid = compare(subParts[0], subParts[1], dataType) < 0;
                }

            } else {
                valid = isValidValueForDataType(parts[i], dataType);
            }

            if ( ! valid ) break;
        }
    }

    if (valid) {
        $(domain).removeClass('invalid');
        $('#new_attr_val_submit').removeAttr('disabled');

    } else {
        if (val.match(/^\s*$/)) $(domain).removeClass('invalid');
        else                    $(domain).addClass('invalid');

        $('#new_attr_val_submit').attr('disabled', true);
    }
}

function ensureValidDateOrBlank(value) {
    return value.match(/^\s*$/) ?
        '' :
        $.datepicker.formatDate('m/d/yy', new Date(Date.parse(value)));
}

function appendToDomain() {
    var arr = [];
    $('.domainPart:visible').each(function() {
        arr.push(this);
    });

    var dataType = getDataType();

    var value;
    if (arr.length >= 1) {
        var start = $(arr[0]).val();

        if ($(arr[0]).hasClass('expr') && (dataType == 'int' || dataType == 'decimal')) {
            start = 'expr{' + start + '}';

        } else if ($(arr[0]).is('input') && (dataType == 'Date' || dataType == 'DateTime')) {
            start = ensureValidDateOrBlank(start);
        }

        var end = '';
        if (arr.length >= 2) {
            end = $(arr[1]).val();

            if ($(arr[1]).hasClass('expr') && (dataType == 'int' || dataType == 'decimal')) {
                end = 'expr{' + end + '}';

            } else if ($(arr[1]).is('input') && (dataType == 'Date' || dataType == 'DateTime')) {
                end = ensureValidDateOrBlank(end);
            }
        }

        if ( ! start.match(/^\s*$/) ) {
            value = start;
            if ( ! end.match(/^\s*$/) && end != start ) value += '-' + end;
        }
    }

    appendToDomainUnlessExists(value);

    $(arr).each(function() {
        if ($(this).is('input')) {
            $(this).val('');
        }
    });

    $(arr[0]).focus();
}

function insertNull() {
    appendToDomainUnlessExists('null');
    validateDomain();
}

function appendToDomainUnlessExists(value) {
    if (value !== undefined) {
        var domain = $('#attr_val_domain');

        var alreadyExists = false;
        $(domain.val().split(/\s*,\s*/)).each(function() {
            if (this == value) {
                alreadyExists = true;
                return false;
            }
        });

        if ( ! alreadyExists ) {
            if (domain.val() != '') {
                value = domain.val() + ', ' + value;
            }

            domain.val(value);

            validateDomain();
        }
    }
}

function appendToTemplate() {
    var charClass = $('#char_class');
    var attrRef = $('#attr_ref');
    var s = attrRef.is(':visible') ? '{' + attrRef.val() + '}' : charClass.val();

    var template = $('#template_value');
    $(template).val($(template).val() + s);

    validateDomainParts();
}

function setInputToggleText(button) {
    var arr = [];
    var activeIndex = -1;
    var index = 0;
    $(button).siblings('td[data-toggle-option]').each(function() {
        arr.push($(this).attr('data-toggle-option'));
        if ($(this).is(':visible')) {
            activeIndex = index;
        }
        index += 1;
    });

    var text = (activeIndex == arr.length - 1) ? arr[0] : arr[activeIndex + 1];
    $(button).text(text);
}

function hideAlternateInputs() {
    $('#domainBuilder').find('.toggle').each(function() {
        var count = 0;
        $(this).siblings('td[data-toggle-option]').each(function() {
            if (count > 0) {
                $(this).addClass('hidden');
            }
            count += 1;
        });
    });
}

function toggleInputSelect(button) {
    var activeIndex = -1;
    var index = 0;
    $(button).siblings('td[data-toggle-option]').each(function() {
        if ($(this).is(':visible')) {
            activeIndex = index;
        }
        index += 1;
    });

    var maxIndex = index - 1;
    var nextIndex = activeIndex == maxIndex ? 0 : activeIndex + 1;

    index = 0;
    $(button).siblings('td[data-toggle-option]').each(function() {
        if (index == nextIndex) {
            $(this).removeClass('hidden');

        } else if ( ! $(this).hasClass('hidden') ) {
            $(this).addClass('hidden');
        }
        index += 1;
    });

    setInputToggleText(button);
    adjustDomainBuilderFormElements();
}

$(document).on('keypress', '.domainPart:visible', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        if ($('#add_to_domain').attr('disabled') === undefined) {
            appendToDomain();
        }
        return false;
    }
});

function insertRef(select, textBox) {
    var s = $(textBox).val().trim();
    if (s.length == 0) {
        $(textBox).val($(select).val());

    } else {
        $(textBox).val(s + ' ' + $(select).val());
    }
}

$(document).on('click', '#domainBuilder .toggle', function() {
    toggleInputSelect(this);
    validateDomainParts();
});

$(document).on('input', 'input.domainPart', function() {
    validateDomainParts();
});

$(document).on('input', 'input#attr_val_domain', function() {   // specifying 'input' in selector to ensure this doesn't
    validateDomain();                                           // fire when input is a select box, as with boolean
});

$(document).on('click', '#insert_ref_min', function() {
    insertRef($('#expr_attr_ref_min'), $('#expr_min'));
    validateDomainParts();
});

$(document).on('click', '#insert_ref_max', function() {
    insertRef($('#expr_attr_ref_max'), $('#expr_max'));
    validateDomainParts();
});

$(document).on('click', '#add_to_template', function() {
    appendToTemplate();
});

$(document).on('click', '#add_to_domain', function() {
    appendToDomain();
});

$(document).on('click', '#insert_null', function() {
    insertNull();
});

var ready = function() {
    enableHover('#domainBuilder .toggle');
    doAttributeValueFormSetup();
};

$(document).ready(ready);
$(document).on('page:load', ready);