/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function getParentAttrId(form) {
    return $(form).parent().attr('data-attr-id');
}

function getConditionAttrId(form) {
    return $(form).children('select.condition_attr').first().val();
}

function getAttrInfo(parentAttrId) {
    return $('#resources').children('div[data-attr-id="' + parentAttrId + '"]');
}

// for getting the data type for an attribute specified in a condition
function getDataTypeForAttribute(parentAttrId, conditionAttrId) {
    return getAttrInfo(parentAttrId)
        .children('div[data-id="' + conditionAttrId + '"]')
        .first()
        .attr('data-type');
}

function getOp(form) {
    return $(form).find('div.condition_op > select').first().val();
}

function setValue(input, value) {
    if ($(input).is('select')) {
        var found = false;
        $(input).children('option').each(function() {
            if ($(this).val() == value) {
                $(this).attr('selected', true);
                found = true;

            } else {
                $(this).removeAttr('selected');
            }
        });

        if ( ! found ) {
            $(input).children('option').first().attr('selected', true);
        }

    } else {
        $(input).val(value);
    }
}

function updateOpSelect(form) {
    var conditionOp = $(form).children('div.condition_op').first();
    var op = $(conditionOp).children().first().val();

    var dataType = getDataTypeForAttribute(getParentAttrId(form), getConditionAttrId(form));
    var select = $('#condition_op_templates').children('div[data-type="' + dataType + '"]');
    $(conditionOp).html(select.get(0).innerHTML);

    setValue($(conditionOp).children('div.condition_op > select').first(), op);
}

function validateValue(input, clearIfInvalid) {
    var form = $(input).closest('form.new_condition');

    var dataType = getDataTypeForAttribute(getParentAttrId(form), getConditionAttrId(form));
    var op = getOp(form);

    var valid = false;

    if (op == 'isnull' || op == 'notnull') {
        valid = true;

    } else if ($(input).is('select')) {
        valid = true;

    } else if ($(input).is('input')) {
        valid = isValidValueForDataType(input, dataType);
    }

    if (valid) {
        $(input).removeClass('invalid');
        $(form).find('input[type="submit"]').first().removeAttr('disabled');

    } else {
        if ($(input).val().match(/^\s*$/)) {
            $(input).removeClass('invalid');

        } else if (clearIfInvalid) {
            setValue(input, '');
            $(input).removeClass('invalid');

        } else {
            $(input).addClass('invalid');
        }

        $(form).find('input[type="submit"]').first().attr('disabled', true);
    }
}

function updateValueInput(form) {
    var conditionVal = $(form).children('div.condition_val').first();
    var value = $(conditionVal).children().first().val();

    var attrId = getConditionAttrId(form);
    var info = getAttrInfo(getParentAttrId(form)).find('div[data-id="' + attrId + '"]').first();
    $(conditionVal).html($(info).children('div.condition_val').first().get(0).innerHTML);
    var input = $(conditionVal).children().first();

    var dataType = $(info).attr('data-type');
    if (dataType == 'Date' || dataType == 'DateTime') {
        $(input).datepicker({
            onSelect: function(date) {
                validateValue(input, false);
            }
        });
    }

    if (value !== undefined) {
        setValue(input, value);
    }
    validateValue(input, true);

    var op = getOp(form);
    if (op == 'isnull' || op == 'notnull') {
        $(conditionVal).hide();
    } else {
        $(conditionVal).show();
    }
}

$(document).on('change', 'select.condition_attr', function() {
    var form = $(this).closest('form.new_condition');
    updateOpSelect(form);
    updateValueInput(form);
});

$(document).on('change', 'div.condition_op > select', function() {
    updateValueInput($(this).closest('form.new_condition'));
});

$(document).on('input', 'div.condition_val > input', function() {
    validateValue(this, false);
});

function refreshConditionForms() {
    $('#attr_vals').find('form.new_condition').each(function() {
        updateOpSelect(this);
        updateValueInput(this);
    });
}

var ready = function() {
    refreshConditionForms();
};

$(document).ready(ready);
$(document).on('page:load', ready);