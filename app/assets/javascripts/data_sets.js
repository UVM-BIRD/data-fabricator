/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function validateEntityName() {
    validateString($('#entity_name'), /^[a-z]\w*$/);  // keep in sync with model validation
}

function validateMinMaxRecs(input) {
    var valid = isIntegerOrRef($(input).val()) && parseInt($(input).val()) >= 1;

    var form = $(input).closest('form.rec_count');

    if (valid) {
        $(input).removeClass('invalid');

        if ($(input).hasClass('min_recs')) {
            var maxInput = $(form).find('input.max_recs').first();
            if (compare($(input).val(), $(maxInput).val(), 'int') > 0) {
                $(maxInput).addClass('invalid');
                valid = false;

            } else {
                $(maxInput).removeClass('invalid');
            }

        } else if ($(input).hasClass('max_recs')) {
            var minInput = $(form).find('input.min_recs').first();
            if (compare($(minInput).val(), $(input).val(), 'int') > 0) {
                $(minInput).addClass('invalid');
                valid = false;

            } else {
                $(minInput).removeClass('invalid');
            }
        }

    } else {
        if ($(input).val().match(/^\s*$/)) {
            $(input).removeClass('invalid');

        } else {
            $(input).addClass('invalid');
        }
    }

    var submit = $(form).find('input[type="submit"]').first();
    if (valid) {
        $(submit).removeAttr('disabled');

    } else {
        $(submit).attr('disabled', true);
    }
}

function buildProgressPoll() {
    var el = $('#build_progress');

    if (el.length) {
        var dataSetId = $('#build_container').attr('data-id');
        var url = el.attr('data-url');
        setTimeout(function() {
            $.ajax({
                url: url,
                type: 'GET',
                success: function(data) {
                    var container = $('#build_container');
                    if (container.attr('data-id') == dataSetId) {
                        container.html(data);
                    }
                },
                dataType: 'html',
                complete: buildProgressPoll(),
                timeout: 2000

            })
        }, 2000);
    }
}

$(document).on('input', '#entity_name', function() {
    validateEntityName();
});

$(document).on('input', 'input.min_recs, input.max_recs', function() {
    validateMinMaxRecs(this);
});

var ready = function() {
    validateEntityName();
    buildProgressPoll();
};

$(document).ready(ready);
$(document).on('page:load', ready);
