/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function validateAttrName() {
    validateString($('#attr_name'), /^[a-z]\w*$/);     // keep in sync with model validation
}

function validateProfileName() {
    validateString($('#profile_name'), /^[a-zA-Z][a-zA-Z0-9 ]*$/);  // keep in sync with model validation
}

$(document).on('input', '#attr_name', function() {
    validateAttrName();
});

$(document).on('input', '#profile_name', function() {
    validateProfileName();
});

$(document).on('click', 'input.transient', function() {
    if ($(this).prop('checked')) {
        $(this).closest('tr').attr('data-transient', '');
    } else {
        $(this).closest('tr').removeAttr('data-transient');
    }
});

var ready = function() {
    validateAttrName();
    validateProfileName();
};

$(document).ready(ready);
$(document).on('page:load', ready);