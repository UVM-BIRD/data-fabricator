# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'logging'

class AttrValsController < ApplicationController
  include Logging

  before_action :authenticate_user!
  before_action :verify_owner!, only: :create
  before_action :set_attr_val, only: [:destroy, :setup_conditions]

  respond_to :html

  # def index
  #   @attr_vals = AttrVal.all
  #   respond_with(@attr_vals)
  # end

  # def show
  #   respond_with(@attr_val)
  # end

  # def new
  #   @attr_val = AttrVal.new
  #   respond_with(@attr_val)
  # end
  #
  # def edit
  # end

  def create
    begin
      attr_val = AttrVal.create(attr_val_params)
      @error = attr_val.errors.values.join('; ') if attr_val.errors.any?

    rescue Exception => e
      @error = e.class.to_s
      log_error e
    end

    do_ajax_response attr_val_params[:attr_id], attr_val_params[:profile_id]
  end

  # def update
  #   @attr_val.update(attr_val_params)
  #   respond_with(@attr_val)
  # end

  def destroy
    attr_id = @attr_val.attr_id
    profile_id = @attr_val.profile_id

    @attr_val.destroy

    do_ajax_response attr_id, profile_id
  end

  def setup_conditions
    respond_to do |format|
      format.js { render layout: false }
    end
  end

  private
    def set_attr_val
      @attr_val = AttrVal.find(params[:id])
    end

    def do_ajax_response(attr_id, profile_id)
      @attr_vals = profile_id.blank? ?
          Attr.find(attr_id).non_profile_attr_vals :
          Profile.find(profile_id).attr_vals

      respond_to do |format|
        format.js { render layout: false }
      end
    end

    def verify_owner!
      attr_id = attr_val_params[:attr_id]
      if attr_id
        attr = Attr.find(attr_id)
        unless attr && attr.entity.data_set.user == current_user
          redirect_to root_url
        end

      else
        redirect_to root_url
      end
    end

    def attr_val_params
      params.require(:attr_val).permit(:attr_id, :profile_id, :domain)
    end
end
