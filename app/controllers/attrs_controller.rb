# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'logging'

class AttrsController < ApplicationController
  include Logging

  before_action :authenticate_user!
  before_action :verify_owner!, only: :create
  before_action :set_attr, only: [:show, :update, :destroy, :toggle_transient]

  respond_to :html

  # def index
  #   @attrs = Attr.all
  #   respond_with(@attrs)
  # end

  def show
    respond_with(@attr)
  end

  # def new
  #   @attr = Attr.new
  #   respond_with(@attr)
  # end

  # def edit
  # end

  def create
    begin
      attr = Attr.create(attr_params)
      @error = attr.errors.values.join('; ') if attr.errors.any?

    rescue Exception => e
      @error = e.class.to_s
      log_error e
    end

    do_ajax_response attr_params[:entity_id]
  end

  def update
    @attr.update(attr_params)
    respond_with(@attr)
  end

  def destroy
    entity_id = @attr.entity_id
    @attr.destroy
    do_ajax_response entity_id
  end

  def setup_profile
    @attr = Attr.find(params[:attr_id])
    respond_to do |format|
      format.js { render layout: false }
    end
  end

  def toggle_transient
    @attr.transient = params[:transient] == 'yes'
    @attr.save
    respond_to do |format|
      format.js { render layout: false }
    end
  end

  private
    def set_attr
      @attr = Attr.find(params[:id])
    end

    def do_ajax_response(entity_id)
      @attrs = Attr.where(entity_id: entity_id)
      respond_to do |format|
        format.js { render layout: false }
      end
    end

    def attr_params
      params.require(:attr).permit(:entity_id, :name, :data_type)
    end

    def verify_owner!
      entity_id = attr_params[:entity_id]
      if entity_id
        entity = Entity.find(entity_id)
        unless entity && entity.data_set.user == current_user
          redirect_to root_url
        end

      else
        redirect_to root_url
      end
    end
end
