# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'logging'

class ConditionsController < ApplicationController
  include Logging

  before_action :authenticate_user!
  before_action :verify_owner!, only: :create
  before_action :set_condition, only: :destroy

  respond_to :html

  # def index
  #   @conditions = Condition.all
  #   respond_with(@conditions)
  # end

  # def show
  #   respond_with(@condition)
  # end

  # def new
  #   @condition = Condition.new
  #   respond_with(@condition)
  # end

  # def edit
  # end

  def create
    begin
      condition = Condition.create(condition_params)
      @error = condition.errors.values.join('; ') if condition.errors.any?

    rescue Exception => e
      @error = e.class.to_s
      log_error e
    end

    do_ajax_response condition_params[:attr_val_id]
  end

  # def update
  #   @condition.update(condition_params)
  #   respond_with(@condition)
  # end

  def destroy
    attr_val_id = @condition.attr_val_id
    @condition.destroy
    do_ajax_response attr_val_id
  end

  private
    def set_condition
      @condition = Condition.find(params[:id])
    end

    def do_ajax_response(attr_val_id)
      @attr_val = AttrVal.find(attr_val_id)
      respond_to do |format|
        format.js { render layout: false }
      end
    end

    def verify_owner!
      attr_val_id = condition_params[:attr_val_id]
      if attr_val_id
        attr_val = AttrVal.find(attr_val_id)
        unless attr_val && attr_val.attr.entity.data_set.user == current_user
          redirect_to root_url
        end

      else
        redirect_to root_url
      end
    end

    def condition_params
      params.require(:condition).permit(:attr_val_id, :attr_id, :negated, :op, :value, :join_using)
    end
end
