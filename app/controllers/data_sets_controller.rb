# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'fileutils'
require 'tmpdir'
require 'securerandom'
require 'zip_util'
require 'executor_pool'
require 'execution_plan'
require 'build_manager'
require 'logging'

class DataSetsController < ApplicationController
  include Logging

  before_action :authenticate_user!
  before_action :set_data_set, only: [:show, :update, :destroy, :build, :retrieve_build, :build_status, :cancel_build]

  respond_to :html

  def index
    @data_sets = DataSet.where(user: current_user)
    respond_with(@data_sets)
  end

  def show
    respond_with(@data_set)
  end

  # def new
  #   @data_set = DataSet.new
  #   respond_with(@data_set)
  # end

  # def edit
  # end

  def create
    begin
      data_set = DataSet.create(data_set_params.merge(user: current_user))
      @error = data_set.errors.values.join('; ') if data_set.errors.any?

    rescue Exception => e
      @error = e.class.to_s
      log_error e
    end

    do_ajax_response
  end

  def update
    @data_set.update(data_set_params)
    respond_with(@data_set)
  end

  def destroy
    @data_set.destroy
    do_ajax_response
  end

  def build
    if @data_set.build_data.nil? || @data_set.build_data.may_rebuild?
      do_build
    end

    respond_to do |format|
      format.js { render layout: false }
    end
  end

  def retrieve_build
    build_data = @data_set.build_data
    if build_data && build_data.status == BuildData::STATUS_COMPLETED
      zip_data = File.read build_export_path.join(build_data.file)
      send_data zip_data, type: 'application/zip', filename: zip_filename_for(@data_set)
      destroy_current_build

    else
      redirect_to @data_set, flash: { error: 'Build does not exist for the specified data set' }
    end
  end

  def build_status
    respond_to do |format|
      format.html { render layout: false }
    end
  end

  def cancel_build
    BuildManager.instance.cancel_build_for current_user, @data_set
    @data_set.build_data.update(status: BuildData::STATUS_CANCELING) if @data_set.build_data

    respond_to do |format|
      format.js { render layout: false }
    end
  end

  private
    def do_build
      destroy_current_build

      export_path = build_export_path
      Dir.mkdir(export_path) unless Dir.exists?(export_path)

      build_data = BuildData.create(
          user:       current_user,
          data_set:   @data_set,
          thread_id:  SecureRandom.hex(5),
          status:     BuildData::STATUS_PREPARING)

      ExecutorPool.instance.execute(thread_id: build_data.thread_id) do
        begin
          Dir.mktmpdir do |dir|
            plan = ExecutionPlan.new(current_user, @data_set)

            build_data.update(status: BuildData::STATUS_RUNNING)

            plan.execute(dir)

            zip_file = ZipUtil.create_zip(dir)
            target_filename = "#{SecureRandom.hex(10)}.zip"

            FileUtils.move zip_file, export_path.join(target_filename)

            build_data.update(status: BuildData::STATUS_COMPLETED, file: target_filename)
          end

        rescue CanceledException
          log.info "thread #{build_data.thread_id} has been canceled."
          build_data.update(status: BuildData::STATUS_CANCELED)

        rescue Exception => e
          log_error e, 'building data'
          build_data.update(status: BuildData::STATUS_ERROR, message: e.message)
        end
      end
    end

    def destroy_current_build
      if @data_set.build_data
        File.delete(build_export_path.join(@data_set.build_data.file)) if @data_set.build_data.file
        @data_set.build_data.destroy
      end
    end

    def build_export_path
      # todo : perhaps the builds path shouldn't be under Rails.root?
      Rails.root.join('builds')
    end

    def zip_filename_for(data_set)
      name = data_set.name.strip
                 .gsub(/[^a-zA-Z0-9_ -]/, '')
                 .gsub(/\s+/, '_')
      "#{name}_data.zip"
    end

    def set_data_set
      @data_set = DataSet.where(user: current_user).find(params[:id])
    end

    def do_ajax_response
      @data_sets = DataSet.where(user: current_user)
      respond_to do |format|
        format.js { render layout: false }
      end
    end

    def data_set_params
      params.require(:data_set).permit(:user_id, :name, :desc)
    end
end
