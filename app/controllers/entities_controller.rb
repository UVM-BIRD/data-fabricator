# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
require 'logging'

class EntitiesController < ApplicationController
  include Logging

  before_action :authenticate_user!
  before_action :verify_owner!, only: :create
  before_action :set_entity, only: [:show, :update, :destroy, :change_parent, :change_rec_count]

  respond_to :html

  # def index
  #   @entities = Entity.all
  #   respond_with(@entities)
  # end

  def show
    respond_with(@entity)
  end

  # def new
  #   @entity = Entity.new
  #   respond_with(@entity)
  # end
  #
  # def edit
  # end

  def create
    begin
      entity = Entity.create(entity_params)
      @error = entity.errors.values.join('; ') if entity.errors.any?

    rescue Exception => e
      @error = e.class.to_s
      log_error e
    end

    do_ajax_response entity_params[:data_set_id]
  end

  def update
    @entity.update(entity_params)
    respond_with(@entity)
  end

  def destroy
    data_set_id = @entity.data_set_id
    @entity.destroy
    do_ajax_response data_set_id
  end

  def change_parent
    parent_entity_id = entity_params[:parent_entity_id]
    do_save = parent_entity_id.blank?
    unless parent_entity_id.blank?
      do_save = Entity.find(parent_entity_id).data_set.user == current_user
    end

    if do_save
      unless parent_entity_id.blank?

        # the following prevents an impossible cyclical hierarchy, such that A -> B and B -> A

        new_parent = Entity.find(parent_entity_id)
        if is_descendant(@entity, new_parent)
          child = branch_along_which_exists(@entity, new_parent)
          child.parent_entity_id = @entity.parent_entity_id
          child.save
        end
      end

      @entity.parent_entity_id = parent_entity_id
      @entity.save
    end

    do_ajax_response @entity.data_set_id
  end

  def change_rec_count
    p = entity_params

    if p[:min_recs] && p[:max_recs]
      min = p[:min_recs].to_i
      max = p[:max_recs].to_i
      @entity.min_recs = min >= 1 ? min : 1
      @entity.max_recs = max >= min ? max : min
      @entity.save
    end

    respond_to do |format|
      format.js { render layout: false }
    end
  end

  private
    def branch_along_which_exists(entity, new_parent)
      entity.branch_along_which_exists new_parent
    end

    def is_descendant(entity, new_parent)
      entity.has_descendant new_parent
    end

    def set_entity
      @entity = Entity.find(params[:id])
    end

    def do_ajax_response(data_set_id)
      @entities = DataSet.find(data_set_id).entities_sorted_hierarchically
      respond_to do |format|
        format.js { render layout: false }
      end
    end

    def entity_params
      params.require(:entity).permit(:data_set_id, :parent_entity_id, :name, :desc, :min_recs, :max_recs)
    end

    def verify_owner!
      data_set_id = entity_params[:data_set_id]
      if data_set_id
        data_set = DataSet.find(data_set_id)
        unless data_set && data_set.user == current_user
          redirect_to root_url
        end

      else
        redirect_to root_url
      end
    end
end
