# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'logging'

class ProfilesController < ApplicationController
  include Logging

  before_action :authenticate_user!
  before_action :verify_owner!, only: :create
  before_action :set_profile, only: [:show, :update, :destroy, :change_proportion, :toggle_required]

  respond_to :html

  # def index
  #   @profiles = Profile.all
  #   respond_with(@profiles)
  # end

  def show
    respond_with(@profile)
  end

  # def new
  #   @profile = Profile.new
  #   respond_with(@profile)
  # end
  #
  # def edit
  # end

  def create
    begin
      profile = Profile.create(profile_params)
      @error = profile.errors.values.join('; ') if profile.errors.any?

    rescue Exception => e
      @error = e.class.to_s
      log_error e
    end

    do_ajax_response profile_params[:entity_id]
  end

  def update
    @profile.update(profile_params)
    respond_with(@profile)
  end

  def destroy
    entity_id = @profile.entity_id
    @profile.destroy
    do_ajax_response entity_id
  end

  def change_proportion
    p = profile_params[:proportion].to_i
    if p >= 0 && p <= 100
      @profile.proportion = profile_params[:proportion]
      @profile.save
    end
    do_ajax_response @profile.entity_id
  end

  def toggle_required
    @profile.required = params[:required] == 'yes'
    @profile.save
    respond_to do |format|
      format.js { render layout: false }
    end
  end

  private
    def set_profile
      @profile = Profile.find(params[:id])
    end

    def do_ajax_response(entity_id)
      @profiles = Profile.where(entity_id: entity_id)
      respond_to do |format|
        format.js { render layout: false }
      end
    end

    def profile_params
      params.require(:profile).permit(:entity_id, :name, :desc, :proportion)
    end

  def verify_owner!
    entity_id = profile_params[:entity_id]
    if entity_id
      entity = Entity.find(entity_id)
      unless entity && entity.data_set.user == current_user
        redirect_to root_url
      end

    else
      redirect_to root_url
    end
  end
end
