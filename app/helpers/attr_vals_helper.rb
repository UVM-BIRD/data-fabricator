# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

module AttrValsHelper
  def str_template_char_class_list
    Attr::STR_TEMPLATE_ESCAPABLES.collect{ |char, hsh|
      [ hsh[:desc], "#{Attr::STR_TEMPLATE_ESCAPE_CHAR}#{char}" ]
    }
  end

  def format_domain_for_display(attr_val)
    parts = []
    attr_val.each_domain_part(unescape: true) do |part|
      parts << part
    end
    parts.join("<br/>\n").html_safe
  end

  private
    def is_usable_in_conditions?(attr)
      attr.data_type != Attr::TYPE_STRING_TEMPLATE
    end
end
