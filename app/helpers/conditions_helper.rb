# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

module ConditionsHelper
  def build_condition_attrs_list(attr_val)
    arr = []
    entity = attr_val.attr.entity
    while entity do
      entity.attrs.each do |attr|
        if is_usable_in_conditions?(attr) && attr != attr_val.attr
          key = entity == attr_val.attr.entity ? attr.name : "#{entity.name}.#{attr.name}"
          arr << [key, attr.id]
        end
      end
      entity = entity.parent
    end
    arr
  end

  def build_condition_join_list
    arr = []
    arr << [ 'and', Condition::JOIN_AND ]
    arr << [ 'or',  Condition::JOIN_OR ]
    arr
  end

  def build_condition_operators_list(data_type)
    arr = []

    if data_type == Attr::TYPE_INT || data_type == Attr::TYPE_DECIMAL || data_type == Attr::TYPE_DATE || data_type == Attr::TYPE_DATETIME
      arr << [ 'is less than',                Condition::OP_LESS_THAN ]
      arr << [ 'is less than or equal to',    Condition::OP_LESS_THAN_OR_EQUAL_TO ]
      arr << [ 'is equal to',                 Condition::OP_EQUAL_TO ]
      arr << [ 'is not equal to',             Condition::OP_NOT_EQUAL_TO ]
      arr << [ 'is greater than or equal to', Condition::OP_GREATER_THAN_OR_EQUAL_TO ]
      arr << [ 'is greater than',             Condition::OP_GREATER_THAN ]

      if data_type == Attr::TYPE_INT || data_type == Attr::TYPE_DECIMAL
        arr << [ 'is evenly divisible by',      Condition::OP_EVENLY_DIVISIBLE_BY ]
        arr << [ 'is not evenly divisible by',  Condition::OP_NOT_EVENLY_DIVISIBLE_BY ]
      end

    elsif data_type == Attr::TYPE_BOOLEAN || data_type == Attr::TYPE_ENUM
      arr << [ 'is',      Condition::OP_EQUAL_TO ]
      arr << [ 'is not',  Condition::OP_NOT_EQUAL_TO ]
    end

    arr << [ 'is null',     Condition::OP_IS_NULL ]
    arr << [ 'is not null', Condition::OP_IS_NOT_NULL ]

    arr
  end

  private
    def is_usable_in_conditions?(attr)
      attr.data_type != Attr::TYPE_STRING_TEMPLATE
    end
end
