# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

module ProfilesHelper
  def build_proportions_list(profile)
    used_by_others = 0
    profile.entity.profiles.each do |p|
      used_by_others += p.proportion unless p == profile || p.proportion.blank?
    end

    max = 100 - used_by_others

    arr = []
    for i in 0 .. max do
      arr << [ "#{i}%", i ]
    end
    arr
  end

  def build_profile_attributes_list(profile)
    profile.entity.attrs.collect { |a| [a.name, a.id] }
  end
end
