# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'build_manager'
require 'logging'

class Attr < ActiveRecord::Base
  include Logging

  belongs_to :entity

  has_many :attr_vals, dependent: :delete_all
  has_many :conditions, dependent: :delete_all

  after_update :adjust_domain_refs

  validates :name, format: { with: /\A[a-z]\w*\z/ }  # keep in sync with entities.js:validateAttrName()

  validate :validate_name

  TYPE_BOOLEAN = 'boolean'
  TYPE_INT = 'int'
  TYPE_DECIMAL = 'decimal'
  TYPE_DATE = 'Date'
  TYPE_DATETIME = 'DateTime'
  TYPE_ENUM = 'Enum'
  TYPE_STRING_TEMPLATE = 'StringTemplate'

  NULL = 'null'

  REGEX_BOOLEAN = /\A(true|false)\z/
  REGEX_ENUM = /\A\w((\\,|[^,])*\w)?\z/

  ZPUN_LEN_PROC = Proc.new { |attr, options|
    options[:totals][attr.entity.name.to_sym][:grand_total].to_s.length + 1
  }

  ZPUN_PROC = Proc.new { |attr, options|
    build_info = BuildManager.instance.get_build_info options[:user], attr.entity.data_set
    build_info[:zpun] = {} unless build_info[:zpun]

    set = build_info[:zpun][attr.id]
    unless set
      set = Set.new
      build_info[:zpun][attr.id] = set
    end

    size = ZPUN_LEN_PROC.call(attr, options)
    pool = Array.new(size, '9').join.to_i

    zpun = "#{rand(pool)}".rjust(size, '0')
    while set.include?(zpun)
      zpun = "#{rand(pool)}".rjust(size, '0')
    end

    set << zpun

    zpun
  }

  STR_TEMPLATE_ESCAPE_CHAR = '\\'
  STR_TEMPLATE_ESCAPABLES = {
      'a'  =>  { desc: 'Lower-case letter (a-z)',   value: ('a'..'z').to_a },
      'A'  =>  { desc: 'Upper-case letter (A-Z)',   value: ('A'..'Z').to_a },
      '0'  =>  { desc: 'Number (0-9)',              value: (0..9).to_a },
      'u'  =>  { desc: 'Zero-padded unique number', value: ZPUN_PROC, len_proc: ZPUN_LEN_PROC },
      '\\' =>  { desc: 'Backslash literal (\)',     value: '\\' },
      ','  =>  { desc: 'Comma literal (,)',         value: ',' },
      '{'  =>  { desc: 'Open brace literal ({)',    value: '{' },
      '}'  =>  { desc: 'Close brace literal (})',   value: '}' }
  }

  # keep this regex in sync with its counterpart in application.js:isStringTemplate(value)
  REGEX_STR_TEMPLATE = /\A(\\[#{STR_TEMPLATE_ESCAPABLES.keys.join}]|\{(?:[a-z]\w*\.)?[a-z]\w*\}|[^\\,{}])+\z/

  DATE_PATTERN = '%m/%d/%Y'


  def to_s
    name
  end

  def self.all_data_types
    [ TYPE_BOOLEAN, TYPE_INT, TYPE_DECIMAL, TYPE_DATE, TYPE_DATETIME, TYPE_ENUM, TYPE_STRING_TEMPLATE ]
  end

  def self.attr_ref_types
    [ TYPE_INT, TYPE_DECIMAL, TYPE_DATE, TYPE_DATETIME, TYPE_STRING_TEMPLATE ]
  end

  def fully_qualified_name
    "#{entity.fully_qualified_name}.#{name}"
  end

  def may_reference_in_domain(attr)
    if self == attr
      false

    elsif data_type == Attr::TYPE_STRING_TEMPLATE ||
          data_type == attr.data_type ||
         (data_type == TYPE_DATETIME && attr.data_type == TYPE_DATE) ||
         (data_type == TYPE_DATE &&     attr.data_type == TYPE_DATETIME) ||
         (data_type == TYPE_DECIMAL &&  attr.data_type == TYPE_INT) ||
         (data_type == TYPE_INT &&      attr.data_type == TYPE_DECIMAL)
      true

    else
      false
    end
  end

  def non_profile_attr_vals
    attr_vals.where(profile_id: nil)
  end

  def sql_data_type(options = {})
    case data_type
      when TYPE_BOOLEAN then          return 'boolean'
      when TYPE_INT then              return 'int'
      when TYPE_DECIMAL then
        precision, scale = determine_precision_scale
        return "decimal(#{precision}, #{scale})"

      when TYPE_DATE then             return 'date'
      when TYPE_DATETIME then         return 'datetime'
      when TYPE_ENUM then             return "varchar(#{sql_data_type_len})"
      when TYPE_STRING_TEMPLATE then  return "varchar(#{sql_data_type_len(options)})"
      else                            raise "unhandled data type '#{data_type}'"
    end
  end

  def sql_data_type_len(options = {})
    if data_type == TYPE_ENUM
      len = 0
      attr_vals.each do |attr_val|
        attr_val.each_domain_part do |domain_val|
          len = domain_val.length if domain_val.length > len
        end
      end
      len

    elsif data_type == TYPE_STRING_TEMPLATE
      len = 0
      attr_vals.each do |attr_val|
        attr_val.each_domain_part do |part|
          if part =~ AttrVal::DOMAIN_STR_TEMPLATE_ATTR_REF
            # string templates that contain attribute references require potentially significant additional processing
            # and so instead, for the time being at least, any attribute value that contains attribute references
            # will have a length that should be sufficiently large to store any likely reasonable values that might
            # be generated.
            len = 255 if len < 255

          else
            dv = part
            STR_TEMPLATE_ESCAPABLES.each_pair do |char, data|
              dv = data[:len_proc] ?
                  dv.gsub("\\#{char}") { Array.new(data[:len_proc].call(self, options), char).join } :
                  dv.gsub("\\#{char}", char)
            end
            len = dv.length if dv.length > len
          end
        end
      end
      len

    else
      nil
    end
  end

  def generate_default_value
    nil
  end

  # cache attr_vals by condition count, as this logic gets called zillions of times.
  def attr_vals_by_condition_count(profile_id = nil)
    @attr_vals_by_cond_ct = {} unless @attr_vals_by_cond_ct

    unless @attr_vals_by_cond_ct[profile_id]
      hsh = {}
      attr_vals.where(profile_id: profile_id).each do |attr_val|
        ct = attr_val.conditions.size
        hsh[ct] = [] unless hsh[ct]
        hsh[ct] << attr_val
      end
      @attr_vals_by_cond_ct[profile_id] = hsh
    end

    @attr_vals_by_cond_ct[profile_id]
  end

  DFLT_PRECISION = 2
  DFLT_SCALE = 1

  def determine_precision_scale
    return 'invalid data type' unless data_type == TYPE_DECIMAL || data_type == TYPE_INT

    if attr_vals.any?
      precision = 0
      scale = 0

      attr_vals.each do |attr_val|
        precision = attr_val.precision if attr_val.precision > precision
        scale = attr_val.scale if attr_val.scale > scale
      end

      return precision, scale

    else
      return DFLT_PRECISION, DFLT_SCALE
    end
  end

  def avail_domain_attr_refs
    hsh = {}

    ent = entity
    while ent do
      ent.attrs.each do |a|
        if may_reference_in_domain(a)
          key = ent == entity ? a.name : "#{ent.name}.#{a.name}"
          hsh[key] = a
        end
      end
      ent = ent.parent
    end

    hsh
  end

  private
    def adjust_domain_refs
      return if name == name_was

      # first handle references by the same entity - these will not include the entity reference
      regex = /\b#{name_was}\b/
      str_template_regex = /(?<!\\)\{#{name_was}\}/

      entity.attrs.each do |attr|
        attr.attr_vals.each do |attr_val|
          if attr.data_type == Attr::TYPE_STRING_TEMPLATE && attr_val.domain =~ str_template_regex
            new_domain = attr_val.domain.gsub(str_template_regex, "{#{name}}")
            attr_val.update(domain: new_domain)

          elsif attr.data_type != Attr::TYPE_STRING_TEMPLATE && attr_val.domain =~ regex
            new_domain = attr_val.domain.gsub(regex, name)
            attr_val.update(domain: new_domain)
          end
        end
      end

      regex = /\b#{entity.name}\.#{name_was}\b/
      str_template_regex = /(?<!\\)\{#{entity.name}\.#{name_was}\}/
      replacement = "#{entity.name}.#{name}"

      entity.each_descendant_entity do |entity|
        entity.attrs.each do |attr|
          attr.attr_vals.each do |attr_val|
            if attr.data_type == Attr::TYPE_STRING_TEMPLATE && attr_val.domain =~ str_template_regex
              new_domain = attr_val.domain.gsub(str_template_regex, "{#{replacement}}")
              attr_val.update(domain: new_domain)

            elsif attr.data_type != Attr::TYPE_STRING_TEMPLATE && attr_val.domain =~ regex
              new_domain = attr_val.domain.gsub(regex, replacement)
              attr_val.update(domain: new_domain)
            end
          end
        end
      end
    end

    def validate_name
      if name.downcase == 'id' || name.downcase == 'null' || (entity.parent && name.downcase == "#{entity.parent.name.downcase}_id")
        errors.add(:name, 'is reserved')
      end
    end
end
