# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'expr_evaluator'
require 'logging'

class AttrVal < ActiveRecord::Base
  include Logging

  belongs_to :attr
  belongs_to :profile

  has_many :conditions, dependent: :delete_all

  before_save :reset_precision_scale

  validate :validate_domain

  validate :ensure_no_cyclical_deps

  @precision = nil
  @scale = nil

  DOMAIN_PART_DELIM = /\s*,\s*/
  DOMAIN_STRING_PART_DELIM = /\s*(?<!\\),\s*/                           # ignore escaped commas; negative lookbehind
  DOMAIN_ATTR_REF = /(?:[a-z]\w*\.)?[a-z]\w*/                           # attribute reference pattern for most data types
  DOMAIN_STR_TEMPLATE_ATTR_REF = /(?<!\\)(\{(?:[a-z]\w*\.)?[a-z]\w*\})/ # attribute reference pattern for string templates

  DOMAIN_RANGE_MATCH = /\s*(expr\{[^}]+\}|[^-]+)\s*(?:-\s*(expr\{[^}]+\}|[^-]+))?\s*/

  def to_s
    domain
  end

  def conditions_as_string
    if conditions.any?
      s = '('
      conditions.each do |c|
        if c.join_using == Condition.JOIN_OR
          s << ') or ('

        elsif c.join_using == Condition.JOIN_AND
          s << ' and '
        end

        s << c.to_s
      end
      s + ')'

    else
      ''
    end
  end

  def matches_condition(hsh)
    if condition_groups.any?
      condition_groups.each do |cg|
        return true if matches_all_conditions(hsh, cg)
      end
      false

    else
      true
    end
  end

  def evaluate(hsh, options = {})
    case attr.data_type
      when Attr::TYPE_BOOLEAN then          eval_boolean
      when Attr::TYPE_INT then              eval_int(hsh)
      when Attr::TYPE_DECIMAL then          eval_decimal(hsh)
      when Attr::TYPE_DATE then             eval_date(hsh)
      when Attr::TYPE_DATETIME then         eval_datetime(hsh)
      when Attr::TYPE_ENUM then             eval_enum
      when Attr::TYPE_STRING_TEMPLATE then  eval_string_template(hsh, options)
      else                                  raise "unhandled data type '#{attr.data_type}'"
    end
  end

  def precision
    return nil unless attr.data_type == Attr::TYPE_DECIMAL || attr.data_type == Attr::TYPE_INT
    determine_precision_scale unless @precision
    @precision
  end

  def scale
    return nil unless attr.data_type == Attr::TYPE_DECIMAL || attr.data_type == Attr::TYPE_INT
    determine_precision_scale unless @scale
    @scale
  end

  def attr_dependencies
    arr = []

    if Attr::attr_ref_types.include? attr.data_type
      refs = attr.avail_domain_attr_refs
      each_domain_part do |part|
        next if part == Attr::NULL

        if attr.data_type == Attr::TYPE_STRING_TEMPLATE
          each_str_template_attr_ref(part) do |ref|
            if refs.key? ref
              arr << refs[ref]

            else
              raise "invalid attribute reference '#{ref}' for #{attr} : #{part}"
            end
          end

        else
          each_attr_ref(part) do |ref|
            if refs.key? ref
              arr << refs[ref]

            else
              raise "invalid attribute reference '#{ref}' for #{attr} : #{part}"
            end
          end
        end
      end
    end

    conditions.each do |c|
      arr << c.attr
    end

    arr.uniq
  end

  def each_domain_part(options = {})
    raise 'block required' unless block_given?

    regex = attr.data_type == Attr::TYPE_STRING_TEMPLATE || attr.data_type == Attr::TYPE_ENUM ?
        DOMAIN_STRING_PART_DELIM :
        DOMAIN_PART_DELIM

    domain.split(regex).each do |part|
      begin
        part = unescape(part) if options[:unescape]
        yield part

      rescue Exception => e
        log_error e, 'processing domain parts'
        raise e
      end
    end
  end

  private

    def unescape(s)
      case attr.data_type
        when Attr::TYPE_ENUM then s.gsub('\,', ',')
        else                      s
      end
    end

    # cache condition groups for performance
    def condition_groups
      @condition_groups = build_condition_groups unless @condition_groups
      @condition_groups
    end

    def each_attr_ref(s)
      raise 'block required' unless block_given?

      s = is_expr?(s) ? unwrap_expr(s) : s
      s.scan(DOMAIN_ATTR_REF) do |ref|
        yield ref
      end
    end

    def each_str_template_attr_ref(s)
      raise 'block required' unless block_given?

      s.scan(DOMAIN_STR_TEMPLATE_ATTR_REF)
          .flatten
          .collect{ |item| item[1 .. item.length - 2] }
          .each do |ref|

        yield ref
      end
    end

    def ensure_no_cyclical_deps
      attr_dependencies.each do |dep_attr|
        traverse_dependent_attrs(dep_attr) do |a|
          if a == attr
            errors.add(:domain, 'detected cyclical dependency')
            break
          end
        end
      end
    end

    def traverse_dependent_attrs(attr, &block)
      raise 'block required' unless block_given?

      attr.attr_vals.each do |attr_val|
        attr_val.attr_dependencies.each do |a|
          block.yield(a)
          traverse_dependent_attrs a, &block
        end
      end
    end

    def validate_domain
      begin
        case attr.data_type
          when Attr::TYPE_BOOLEAN then          validate_boolean_domain
          when Attr::TYPE_INT then              validate_numeric_domain
          when Attr::TYPE_DECIMAL then          validate_numeric_domain
          when Attr::TYPE_DATE then             validate_date_domain
          when Attr::TYPE_DATETIME then         validate_datetime_domain
          when Attr::TYPE_ENUM then             validate_enum_domain
          when Attr::TYPE_STRING_TEMPLATE then  validate_str_template_domain
          else                                  raise "unhandled data type '#{attr.data_type}'"
        end

      rescue
        errors.add(:domain, 'is invalid')
      end
    end

    def validate_boolean_domain
      each_domain_part do |part|
        raise unless part == Attr::NULL || part =~ Attr::REGEX_BOOLEAN
      end
    end

    def validate_numeric_domain
      refs = attr.avail_domain_attr_refs.keys

      each_domain_part do |part|
        min, max = domain_range_parts part
        next if min == Attr::NULL

        if is_expr? min
          validate_expr substitute_refs(min, refs)
        else
          Float(min) unless refs.include?(min)
        end

        if max
          if is_expr? max
            validate_expr substitute_refs(max, refs)
          else
            Float(max) unless refs.include?(max)
          end

          unless has_references?(min, refs) || has_references?(max, refs)
            min_val = is_expr?(min) ? eval_expr(min) : min.to_f
            max_val = is_expr?(max) ? eval_expr(max) : max.to_f
            raise unless min_val >= 0 && min_val < max_val
          end
        end
      end
    end

    def substitute_refs(expr, refs)
      expr = unwrap_expr(expr)

      if refs.is_a? Array
        refs.each do |ref|
          expr.gsub!(ref, '1') if expr.include?(ref)
        end

      elsif refs.is_a? Hash
        refs.each_pair do |key, value|
          if expr.include? key
            return nil if value.nil?
            expr.gsub!(key, value.to_s)
          end
        end
      end

      "expr{#{expr}}"
    end
  
    def has_references?(domain_part, refs)
      domain_part = unwrap_expr(domain_part) if is_expr?(domain_part)
      refs.each do |ref|
        return true if domain_part.include?(ref)
      end
      false
    end

    def is_expr?(s)
      s != nil && s =~ /\Aexpr\{[^}]+\}\z/
    end

    def unwrap_expr(expr)
      /\Aexpr\{([^}]+)\}\z/.match(expr)[1]
    end

    def validate_expr(expr)
      ExprEvaluator.instance.validate(unwrap_expr(expr))
    end

    def eval_expr(expr)
      expr == nil ?
          nil :
          ExprEvaluator.instance.evaluate(unwrap_expr(expr))
    end

    def validate_date_domain
      refs = attr.avail_domain_attr_refs.keys

      each_domain_part do |part|
        min, max = domain_range_parts part
        next if min == Attr::NULL

        min_date = refs.include?(min) ? true : Date.strptime(min, Attr::DATE_PATTERN)
        if max
          max_date = refs.include?(max) ? true : Date.strptime(max, Attr::DATE_PATTERN)

          unless refs.include?(min) || refs.include?(max)
            raise unless min_date < max_date
          end
        end
      end
    end

    def validate_datetime_domain
      validate_date_domain
    end

    def validate_enum_domain
      each_domain_part do |part|
        raise unless part == Attr::NULL || part =~ Attr::REGEX_ENUM
      end
    end

    def validate_str_template_domain
      refs = attr.avail_domain_attr_refs.keys

      each_domain_part do |part|
        next if part == Attr::NULL

        raise unless part =~ Attr::REGEX_STR_TEMPLATE

        each_str_template_attr_ref(part) do |ref|
          raise unless refs.include?(ref)
        end
      end
    end

    def reset_precision_scale
      @precision = nil
      @scale = nil
    end

    def determine_precision_scale
      refs_hsh = attr.avail_domain_attr_refs
      refs = refs_hsh.keys
      refs_to_check = []

      max_left = 0
      max_right = 0

      arr = []
      each_domain_part do |part|
        min, max = domain_range_parts part

        if is_expr? min             # sort of hack :
          max_left = 10             # determining the resultant precision and scale of a variable expression is really hard,
          max_right = 6             # and there's not a lot of benefit to knowing the precise details.  figure if the person
          break                     # is using an expression as a possible value, they should be alright with these defaults

        elsif refs.include?(min)
          refs_to_check << min

        else
          arr << min
        end

        if max
          if is_expr? max           # sort of hack :
            max_left = 10           # same here as above.
            max_right = 6
            break

          elsif refs.include?(max)
            refs_to_check << max

          else
            arr << max
          end
        end
      end

      # determine precision and scale for all numbers identified in the domain
      arr.each do |n|
        if n.include? '.'
          left_len, right_len = n.split('.').collect{ |s| s.length }
          max_left = left_len if max_left < left_len
          max_right = right_len if max_right < right_len

        else
          max_left = n.length if max_left < n.length
        end
      end

      p = max_left + max_right
      s = max_right

      # now iterate through any attribute references found in the domain, and incorporate their
      # info into the precision and scale for this attribute
      refs_to_check.each do |ref|
        ref_p, ref_s = refs_hsh[ref].determine_precision_scale
        p = ref_p if p < ref_p
        s = ref_s if s < ref_s
      end

      @precision = p
      @scale = s
    end

    def eval_boolean
      parts = domain.split(DOMAIN_PART_DELIM)
      val = parts[rand(parts.size)]

      case val
        when 'true' then      true
        when 'false' then     false
        when Attr::NULL then  nil
        else                  raise 'invalid boolean domain'
      end
    end

    def eval_int(hsh)
      parts = domain.split(DOMAIN_PART_DELIM)
      val = parts[rand(parts.size)]

      return nil if val == Attr::NULL

      min, max = domain_range_parts val

      if is_expr? min
        min = eval_expr(substitute_refs(min, hsh))
      else
        min = hsh.key?(min) ? hsh[min] : min.to_i
      end

      if max
        if is_expr? max
          max = eval_expr(substitute_refs(max, hsh))
        else
          max = hsh.key?(max) ? hsh[max] : max.to_i
        end

        if min.nil? || max.nil?
          nil

        elsif max >= min
          (min + rand(max - min)).to_i      # ensure resulting value is int - could be decimal

        else
          raise "max < min for val : #{val}"
        end

      else
        min
      end
    end

    def eval_decimal(hsh)
      parts = domain.split(DOMAIN_PART_DELIM)
      val = parts[rand(parts.size)]

      return nil if val == Attr::NULL

      min, max = domain_range_parts val

      if is_expr? min
        min = eval_expr(substitute_refs(min, hsh))
      else
        min = hsh.key?(min) ? hsh[min] : min.to_f
      end

      if max
        if is_expr? max
          max = eval_expr(substitute_refs(max, hsh))
        else
          max = hsh.key?(max) ? hsh[max] : max.to_f
        end

        if min.nil? || max.nil?
          nil

        elsif max >= min
          diff = (max - min) * (10 ** scale)
          min + (rand(diff.to_i).to_f / (10 ** scale))

        else
          raise "max < min for val : #{val}"
        end

      else
        min == nil ?
            nil :
            min.to_f.round(scale)
      end
    end

    def eval_date(hsh)
      dt = eval_datetime(hsh)
      dt == nil ?
          nil :
          dt.to_date
    end

    def eval_datetime(hsh)
      parts = domain.split(DOMAIN_PART_DELIM)
      val = parts[rand(parts.size)]

      return nil if val == Attr::NULL

      min, max = domain_range_parts val

      min = hsh.key?(min) ? hsh[min] : Date.strptime(min, Attr::DATE_PATTERN)

      if max
        max = hsh.key?(max) ? hsh[max] : Date.strptime(max, Attr::DATE_PATTERN)

        if min.nil? || max.nil?
          nil

        else
          diff = max - min

          if diff >= 0
            numerator_mult = 86400 / diff.denominator
            min.to_datetime + Rational(rand(diff.numerator * numerator_mult), 86400)

          else
            raise "max < min for val : #{val}"
          end
        end

      else
        min == nil ?
            nil :
            min.to_datetime
      end
    end

    def eval_enum
      parts = domain.split(DOMAIN_STRING_PART_DELIM)
      val = parts[rand(parts.size)]

      val == Attr::NULL ?
          nil :
          unescape(val)
    end

    def eval_string_template(hsh, options = {})
      parts = domain.split(DOMAIN_STRING_PART_DELIM)
      val = parts[rand(parts.size)]

      if val == Attr::NULL
        nil

      else
        val.gsub!(DOMAIN_STR_TEMPLATE_ATTR_REF) {
          ref = $1[1 .. $1.length - 2]                    # remove opening and closing braces from match text
          if hsh.key?(ref)
            hsh[ref]

          else
            raise "invalid attribute reference '#{ref}' for #{attr} : #{val}"
          end
        }

        Attr::STR_TEMPLATE_ESCAPABLES.each_pair do |char, info|
          r = info[:value]
          val.gsub!("#{Attr::STR_TEMPLATE_ESCAPE_CHAR}#{char}") {
            case r
              when Array then r[rand(r.size)]
              when Proc then  r.call(attr, options)
              else            r
            end
          }
        end
        val
      end
    end

    def matches_all_conditions(hsh, conditions)
      conditions.each do |c|
        begin
          return false unless c.matches(hsh)

        rescue Exception => e
          log.error "caught #{e.class.name} matching condition {#{c}} with data #{hsh}"
          raise e
        end
      end

      true
    end

    # group conditions linked by AND; split groups on OR
    def build_condition_groups
      groups = []
      current_group = []

      conditions.each_with_index do |c, i|
        if i > 0 && c.join_using == Condition::JOIN_OR
          groups << current_group
          current_group = []
        end

        current_group << c
      end

      groups << current_group

      groups
    end

    def domain_range_parts(s)
      match = DOMAIN_RANGE_MATCH.match(s)
      arr = [ match[1].strip ]
      arr << match[2].strip if match[2]
      arr
    end
end
