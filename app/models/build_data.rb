# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'build_manager'

class BuildData < ActiveRecord::Base
  belongs_to :user
  belongs_to :data_set

  STATUS_PREPARING = 'PREPARING'
  STATUS_RUNNING = 'RUNNING'
  STATUS_COMPLETED = 'COMPLETED'
  STATUS_ERROR = 'ERROR'
  STATUS_CANCELING = 'CANCELING'
  STATUS_CANCELED = 'CANCELED'

  def current_build_info
    status == STATUS_RUNNING ?
        BuildManager.instance.get_build_info(user, data_set) :
        nil
  end

  def may_rebuild?
    status == STATUS_ERROR || status == STATUS_CANCELED || status == STATUS_COMPLETED
  end
end
