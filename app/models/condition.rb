# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

class Condition < ActiveRecord::Base
  belongs_to :attr_val
  belongs_to :attr

  JOIN_OR = 'or'
  JOIN_AND = 'and'

  OP_IS_NULL = 'isnull'
  OP_IS_NOT_NULL = 'notnull'
  OP_LESS_THAN = 'lt'
  OP_LESS_THAN_OR_EQUAL_TO = 'le'
  OP_EQUAL_TO = 'eq'
  OP_NOT_EQUAL_TO = 'ne'
  OP_GREATER_THAN_OR_EQUAL_TO = 'ge'
  OP_GREATER_THAN = 'gt'
  OP_EVENLY_DIVISIBLE_BY = 'ed'
  OP_NOT_EVENLY_DIVISIBLE_BY = 'nd'

  COMMON_OPS =      [ OP_IS_NULL, OP_IS_NOT_NULL, OP_EQUAL_TO, OP_NOT_EQUAL_TO ]
  COMPARABLE_OPS =  [ OP_LESS_THAN, OP_LESS_THAN_OR_EQUAL_TO, OP_GREATER_THAN_OR_EQUAL_TO, OP_GREATER_THAN ]
  NUMBER_ONLY_OPS = [ OP_EVENLY_DIVISIBLE_BY, OP_NOT_EVENLY_DIVISIBLE_BY ]

  DATA_TYPE_OPS = { Attr::TYPE_BOOLEAN =>         COMMON_OPS,
                    Attr::TYPE_INT =>             COMMON_OPS + COMPARABLE_OPS + NUMBER_ONLY_OPS,
                    Attr::TYPE_DECIMAL =>         COMMON_OPS + COMPARABLE_OPS + NUMBER_ONLY_OPS,
                    Attr::TYPE_DATE =>            COMMON_OPS + COMPARABLE_OPS,
                    Attr::TYPE_DATETIME =>        COMMON_OPS + COMPARABLE_OPS,
                    Attr::TYPE_ENUM =>            COMMON_OPS,
                    Attr::TYPE_STRING_TEMPLATE => []
  }

  before_validation :clear_nullop_value

  validate :validate_op
  validate :validate_value

  validates :join_using, inclusion: { in: [ JOIN_OR, JOIN_AND ] },
            allow_nil: true
  
  validate :ensure_no_cyclical_deps


  def to_s(options = {})
    arr = []
    arr << join_using unless join_using.blank? || options[:hide_join]

    if attr.entity == attr_val.attr.entity
      arr << attr.name

    else
      arr << "#{attr.entity.name}.#{attr.name}"
    end

    arr << op
    arr << value

    arr.join ' '
  end

  def matches(hsh)
    k = attr.entity == attr_val.attr.entity ?
        attr.name :
        "#{attr.entity.name}.#{attr.name}"

    if hsh.key? k
      v = hsh[k]

      if (op == OP_IS_NULL && v.nil?) || (op == OP_IS_NOT_NULL && ! v.nil?)
        match = true

      elsif (op == OP_IS_NULL && ! v.nil?) || (op == OP_IS_NOT_NULL && v.nil?)
        match = false

      else
        match = case attr.data_type
          when Attr::TYPE_BOOLEAN then  matches_boolean(v)
          when Attr::TYPE_INT then      matches_int(v)
          when Attr::TYPE_DECIMAL then  matches_decimal(v)
          when Attr::TYPE_DATE then     matches_date(v)
          when Attr::TYPE_DATETIME then matches_datetime(v)
          when Attr::TYPE_ENUM then     matches_enum(v)
          else raise "cannot evaluate #{k} - unhandled data type '#{attr.data_type}'"
        end
      end

      match

    else
      raise "cannot evaluate #{k} - value is missing"
    end
  end

  private

    def clear_nullop_value
      self.value = nil if op == OP_IS_NULL || op == OP_IS_NOT_NULL
    end

    def validate_op
      unless DATA_TYPE_OPS[attr.data_type].include?(op)
        errors.add(:op, "is invalid for data type '#{attr.data_type}'")
      end
    end

    def validate_value
      unless op == OP_IS_NULL || op == OP_IS_NOT_NULL
        begin
          case attr.data_type
            when Attr::TYPE_BOOLEAN then          raise unless value =~ Attr::REGEX_BOOLEAN
            when Attr::TYPE_INT then              Integer(value)
            when Attr::TYPE_DECIMAL then          Float(value)
            when Attr::TYPE_DATE then             Date.strptime(value, Attr::DATE_PATTERN)
            when Attr::TYPE_DATETIME then         Date.strptime(value, Attr::DATE_PATTERN)  # same as TYPE_DATE, for now
            when Attr::TYPE_ENUM then             raise unless value =~ Attr::REGEX_ENUM
            when Attr::TYPE_STRING_TEMPLATE then  raise unless value =~ Attr::REGEX_STR_TEMPLATE
            else                                  raise "unhandled data type '#{attr.data_type}'"
          end
        rescue
          errors.add(:value, 'is invalid')
        end
      end
    end

    def matches_boolean(v)
      b = case value
        when 'true' then  true
        when 'false' then false
        else              raise "invalid boolean value '#{value}'"
      end

      case op
        when OP_EQUAL_TO then     v == b
        when OP_NOT_EQUAL_TO then v != b
        else                      raise "unhandled boolean operation '#{op}'"
      end
    end

    def matches_int(v)
      i = value.to_i
      case op
        when OP_LESS_THAN then                v < i
        when OP_LESS_THAN_OR_EQUAL_TO then    v <= i
        when OP_EQUAL_TO then                 v == i
        when OP_NOT_EQUAL_TO then             v != i
        when OP_GREATER_THAN_OR_EQUAL_TO then v >= i
        when OP_GREATER_THAN then             v > i
        when OP_EVENLY_DIVISIBLE_BY then      v % i == 0
        when OP_NOT_EVENLY_DIVISIBLE_BY then  v % i != 0
        else                                  raise "unhandled int operation '#{op}'"
      end
    end

    def matches_decimal(v)
      f = value.to_f
      case op
        when OP_LESS_THAN then                v < f
        when OP_LESS_THAN_OR_EQUAL_TO then    v <= f
        when OP_EQUAL_TO then                 v == f
        when OP_NOT_EQUAL_TO then             v != f
        when OP_GREATER_THAN_OR_EQUAL_TO then v >= f
        when OP_GREATER_THAN then             v > f
        when OP_EVENLY_DIVISIBLE_BY then      (v % f).round(10) == 0
        when OP_NOT_EVENLY_DIVISIBLE_BY then  (v % f).round(10) != 0
        else                                  raise "unhandled decimal operation '#{op}'"
      end
    end

    def matches_date(v)
      d = Date.strptime(value, Attr::DATE_PATTERN)
      case op
        when OP_LESS_THAN then                v < d
        when OP_LESS_THAN_OR_EQUAL_TO then    v <= d
        when OP_EQUAL_TO then                 v == d
        when OP_NOT_EQUAL_TO then             v != d
        when OP_GREATER_THAN_OR_EQUAL_TO then v >= d
        when OP_GREATER_THAN then             v > d
        else                                  raise "unhandled date operation '#{op}'"
      end
    end

    def matches_datetime(v)
      d = Date.strptime(value, Attr::DATE_PATTERN)
      start_of_day = DateTime.new(d.year, d.month, d.day, 0, 0, 0)
      end_of_day = DateTime.new(d.year, d.month, d.day, 23, 59, 59)
      case op
        when OP_LESS_THAN then                v < start_of_day
        when OP_LESS_THAN_OR_EQUAL_TO then    v <= end_of_day
        when OP_EQUAL_TO then                 v >= start_of_day && v <= end_of_day
        when OP_NOT_EQUAL_TO then             v < start_of_day || v > end_of_day
        when OP_GREATER_THAN_OR_EQUAL_TO then v >= start_of_day
        when OP_GREATER_THAN then             v > end_of_day
        else                                  raise "unhandled datetime operation '#{op}'"
      end
    end

    def matches_enum(v)
      case op
        when OP_EQUAL_TO then     v == value
        when OP_NOT_EQUAL_TO then v != value
        else                      raise "unhandled enum operation '#{op}'"
      end
    end

    def ensure_no_cyclical_deps
      traverse_dependent_attrs(attr) do |a|
        if a == attr_val.attr
          errors.add(:attr, 'detected cyclical dependency')
          break
        end
      end
    end

    def traverse_dependent_attrs(attr, &block)
      raise 'block required' unless block_given?

      attr.attr_vals.each do |attr_val|
        attr_val.attr_dependencies.each do |a|
          block.yield(a)
          traverse_dependent_attrs a, &block
        end
      end
    end
end
