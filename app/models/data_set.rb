# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

class DataSet < ActiveRecord::Base
  belongs_to :user

  has_many  :entities,    dependent: :delete_all
  has_one   :build_data,  dependent: :delete

  validates :name, format: { with: /\A[a-zA-Z]\w*\z/ }  # keep in sync with validateDataSetName() in home.js

  def to_s
    name
  end

  def entities_sorted_hierarchically(parent_entity_id = nil)
    arr = []
    entities.where(parent_entity_id: parent_entity_id).order(:name).each do |entity|
      arr << entity
      arr << entities_sorted_hierarchically(entity.id)
    end
    arr.flatten
  end

  def create_database_stmt
    "drop database if exists #{name};\ncreate database #{name};\nuse #{name};\n"
  end
end
