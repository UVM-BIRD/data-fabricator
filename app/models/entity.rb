# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'logging'

class Entity < ActiveRecord::Base
  include Logging

  belongs_to :data_set
  belongs_to :parent, foreign_key: 'parent_entity_id', class_name: 'Entity'

  has_many :children, foreign_key: 'parent_entity_id', class_name: 'Entity', dependent: :nullify
  has_many :attrs, dependent: :delete_all
  has_many :profiles, dependent: :delete_all

  after_update :adjust_domain_refs
  before_save :adjust_parent_entity_id

  validates :name, format: { with: /\A[a-z]\w*\z/ }  # keep in sync with validateEntityName() in data_sets.js
  validates :min_recs, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validates :max_recs, numericality: { only_integer: true, greater_than_or_equal_to: 1 }

  validate :validate_name
  validate :max_recs_gte_min_recs

  def to_s
    name
  end

  def fully_qualified_name
    "#{data_set.name}:#{name}"
  end

  def branch_along_which_exists(entity)
    while entity.parent
      if entity.parent == self
        return entity

      else
        entity = entity.parent
      end
    end

    nil
  end

  def has_descendant(entity)
    branch_along_which_exists(entity) != nil
  end

  def create_table_stmt(options = {})
    arr = [ 'id int not null primary key' ]

    arr << "#{parent.name}_id int not null" if parent

    attrs.each do |a|
      arr << "#{a.name} #{a.sql_data_type(options)}" unless a.transient?
    end

    if parent
      arr << "INDEX fk_index (#{parent.name}_id)"
      arr << "FOREIGN KEY (#{parent.name}_id) REFERENCES #{parent.name}(id) ON DELETE CASCADE"
    end

    "create table #{name} (#{arr.join(', ')});\n"
  end

  def csv_headers
    "#{ordered_attrs_for_export.join(',')}\n"
  end

  def ordered_attrs_for_export
    unless @export_attrs
      arr = [ 'id' ]

      arr << "#{parent.name}_id" if parent

      attrs.each do |a|
        arr << a.name unless a.transient?
      end

      @export_attrs = arr.collect{ |item| item.to_sym }
    end

    @export_attrs
  end

  def fabricate_record(profile_id, parent = nil, options = {})
    rec = {}
    rec["#{parent[:entity_key]}_id".to_sym] = parent[:rec][:id] if parent

    attrs_in_eval_order(profile_id).each do |attr|
      attr_vals_hsh = attr.attr_vals_by_condition_count(profile_id)

      if profile_id && attr_vals_hsh.empty?
        # profile exists and is active, but does not override this attribute.
        # use profile-less value domain, if any exist
        attr_vals_hsh = attr.attr_vals_by_condition_count
      end

      if attr_vals_hsh.any?
        hsh = format_attrs_for_evaluation(rec, parent)
        val = nil

        # process tiers of attr_vals, where each tier is identified by the number of conditions each attr_val
        # has.  highest tiers - those with the most conditions - are processed first.  as soon as one attr_val
        # matches, it *and all other matching attr_val items from the same tier* are collected.  later, if there
        # are multiple matching attr_val items, then one is picked at random.  this will ensure that if there
        # are multiple attr_vals with no conditions (which will always match), that any of them may be used.
        # without this, only the first matching attr_val would be used.

        matching_attr_vals = []
        attr_vals_hsh.keys.sort.reverse.each do |k|
          attr_vals_hsh[k].each do |attr_val|
            matching_attr_vals << attr_val if attr_val.matches_condition(hsh)
          end
          break if matching_attr_vals.any?
        end

        if matching_attr_vals.any?
          attr_val = matching_attr_vals[rand(matching_attr_vals.size)]
          val = attr_val.evaluate(hsh, options)
        end

        rec[attr.name.to_sym] = val

      else
        # no user-configured value domains exist.  use the default value domain
        rec[attr.name.to_sym] = attr.generate_default_value
      end
    end

    rec
  end

  def each_descendant_entity(entity = nil, &block)
    raise 'block required' unless block_given?

    entity = self if entity.nil?

    entity.children.each do |child|
      yield child
      each_descendant_entity(child, &block)
    end
  end

  private
    def adjust_domain_refs
      return if name == name_was

      attr_names = attrs.collect{ |a| a.name }.join('|')
      regex = /\b#{name_was}\.(#{attr_names})\b/
      str_template_regex = /(?<!\\)\{#{name_was}\.(#{attr_names})\}/

      # only descendant entities will have domain attribute references that include the entity name component
      each_descendant_entity do |entity|
        entity.attrs.each do |attr|
          next unless Attr::attr_ref_types.include?(attr.data_type)

          attr.attr_vals.each do |attr_val|
            if attr.data_type == Attr::TYPE_STRING_TEMPLATE && attr_val.domain =~ str_template_regex
              new_domain = attr_val.domain.gsub(str_template_regex, "{#{name}.\\1}")
              attr_val.update(domain: new_domain)

            elsif attr.data_type != Attr::TYPE_STRING_TEMPLATE && attr_val.domain =~ regex
              new_domain = attr_val.domain.gsub(regex, "#{name}.\\1")
              attr_val.update(domain: new_domain)
            end
          end
        end
      end
    end

    # generate a hash containing all generated attributes and values for this record
    # and its parent entities, structured in such a way that can be easily consumed by
    # the condition-matching logic
    def format_attrs_for_evaluation(rec, parent)
      hsh = {}

      rec.each_pair do |attr_name, attr_val|
        hsh[attr_name.to_s] = attr_val unless attr_name == :id
      end

      while parent
        parent[:rec].each_pair do |attr_name, attr_val|
          hsh["#{parent[:entity_key]}.#{attr_name}"] = attr_val unless attr_name == :id
        end
        parent = parent[:parent]
      end

      hsh
    end

    # sort attributes in the order in which they should be evaluated.  ensure that attributes that depend on others
    # are populated after their dependencies have been generated.
    # this information is cached to improve performance, and will remain as long as this instance of the entity is used
    def attrs_in_eval_order(profile_id)
      @attrs_in_eval_order = {} unless @attrs_in_eval_order

      unless @attrs_in_eval_order[profile_id]
        arr = []        # final list of attributes to send back
        dep_arr = []    # list of attributes with dependencies

        attrs.each do |attr|
          attr_vals = attr.attr_vals.where(profile_id: profile_id)
          attr_vals = attr.attr_vals if profile_id && attr_vals.empty?

          dependencies = []
          attr_vals.each do |attr_val|
            attr_val.attr_dependencies.each do |a|
              dependencies << a if a.entity == self
            end
          end
          dependencies.uniq!

          if dependencies.any?
            dep_arr << { attr: attr, dependencies: dependencies }

          else
            arr << attr     # no dependencies, so add to final list
          end
        end

        # process attributes with dependencies, adding them at the end ONLY if all dependencies exist in the list
        while dep_arr.any?
          dep_arr.each_index do |i|
            hsh = dep_arr[i]
            all_exist = true

            hsh[:dependencies].each do |attr|
              unless arr.include? attr
                all_exist = false
                break
              end
            end

            if all_exist
              arr << hsh[:attr]
              dep_arr.delete_at(i)
            end
          end
        end

        @attrs_in_eval_order[profile_id] = arr
      end

      @attrs_in_eval_order[profile_id]
    end

    def adjust_parent_entity_id
      self.parent_entity_id = nil if parent_entity_id.blank?
    end

    def validate_name
      if name.downcase == 'null'
        errors.add(:name, 'is reserved')
      end
    end

    def max_recs_gte_min_recs
      if min_recs > max_recs
        errors.add(:max_recs, 'must be greater than or equal to min_recs')
      end
    end
end
