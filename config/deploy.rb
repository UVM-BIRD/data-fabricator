# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'dataFabricator'
set :repo_url, "git@bitbucket.com:UVM-BIRD/#{fetch :application}.git"
set :deploy_to, "/home/rails/#{fetch :application}"

set :branch, :master
set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :keep_releases, 2


# useful for debugging!
namespace :env do
  task :echo do
    run 'echo printing out cap info on remote server'
    run 'printenv'
  end
end

namespace :deploy do
  desc 'Build WAR'
  task :build_war do
    on roles(:app) do
      within release_path do
        with rails_env: :production do
          execute :bundle, :exec, :warble
        end
      end
    end
  end

  desc 'Deploy WAR'
  task :deploy_war do
    on roles(:app) do
      within release_path do
        with rails_env: :production do
          execute :cp, "#{fetch :application}.war", '/usr/share/tomcat6/webapps/'
        end
      end
    end
  end

  desc 'Seed the database'
  task :db_seed do
    on roles(:app) do
      within release_path do
        with rails_env: :production do
          execute :bundle, :exec, :rake, 'db:seed'
        end
      end
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :updated, :db_seed
  after :updated, :build_war
  after :build_war, :deploy_war

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end
