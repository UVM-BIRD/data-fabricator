require 'java'

# this initializer is responsible for auto-requiring all Java dependencies in /lib/java
#
# see:  http://kenai.com/projects/jruby/pages/CallingJavaFromJRuby#Accessing_and_Importing_Java_Classes
# also: http://stackoverflow.com/questions/8110277/how-do-i-correctly-add-to-the-classpath-in-a-jruby-on-rails-project

#puts "Processing JAR dependencies :"
Dir[Rails.root.join('lib', 'java', '*.jar')].each do |jar|
  require jar
end

