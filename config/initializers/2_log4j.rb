# configure log4j logging

if RUBY_PLATFORM =~ /java/
  require 'java'
  require 'log4j_adapter'
  org.apache.logging.log4j.core.config.Configurator.initialize(nil, Rails.root.join('config', 'log4j2.xml').to_s)
  Rails.logger = Log4jAdapter.new 'system'
end
