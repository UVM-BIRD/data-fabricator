# manually load libs for production environment, where lib auto-loading is disabled for thread-safety

Dir[Rails.root.join('lib', '**', '*.rb')].each do |rb|
  require rb
end

Dir[Rails.root.join('app', 'models', '*.rb')].each do |rb|
  require rb
end
