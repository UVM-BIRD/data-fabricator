require 'executor_pool'
require 'build_data'

at_exit do
  ExecutorPool.instance.shutdown
  BuildData.destroy_all
end