Rails.application.routes.draw do
  authenticated :user do
    root to: 'data_sets#index', as: 'authenticated_root'
  end

  root 'home#index'

  get 'home/index'

  devise_for :users

  resources :data_sets, only: [ :index, :show, :create, :update, :destroy ] do
    post :build, on: :member
    post :retrieve_build, on: :member
    get :build_status, on: :member
    post :cancel_build, on: :member
  end

  resources :profiles, only: [ :show, :create, :update, :destroy ] do
    post :change_proportion, on: :member
    post :toggle_required, on: :member
  end

  resources :entities, only: [ :show, :create, :update, :destroy ] do
    post :change_parent, on: :member
    post :change_rec_count, on: :member
  end

  resources :attrs, only: [ :show, :create, :update, :destroy ] do
    post :toggle_transient, on: :member
    post :setup_profile, on: :member
  end

  resources :attr_vals, only: [ :create, :destroy ] do
    post :setup_conditions, on: :member
  end

  resources :conditions, only: [ :create, :destroy ]


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
