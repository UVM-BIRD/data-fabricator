class CreateDataSets < ActiveRecord::Migration
  def change
    create_table :data_sets do |t|
      t.references  :user, index: true
      t.string      :name, limit: 50
      t.string      :desc, limit: 500

      t.timestamps
    end

    add_index :data_sets, [:user_id, :name], unique: true
  end
end
