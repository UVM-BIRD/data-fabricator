class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.references  :data_set,      index: true
      t.references  :parent_entity, index: true
      t.string      :name,          limit: 30
      t.string      :desc,          limit: 500
      t.integer     :min_recs,      default: 1
      t.integer     :max_recs,      default: 1

      t.timestamps
    end

    add_index :entities, [:data_set_id, :name], unique: true
  end
end
