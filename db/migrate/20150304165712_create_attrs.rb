class CreateAttrs < ActiveRecord::Migration
  def change
    create_table :attrs do |t|
      t.references  :entity,    index: true
      t.string      :name,      limit: 30
      t.string      :data_type, limit: 30
      t.boolean     :transient, default: false

      t.timestamps
    end

    add_index :attrs, [:entity_id, :name], unique: true
  end
end
