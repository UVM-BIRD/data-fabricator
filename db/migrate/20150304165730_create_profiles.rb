class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references  :entity,      index: true
      t.string      :name,        limit: 50
      t.string      :desc,        limit: 500
      t.integer     :proportion,  default: 0
      t.boolean     :required,    default: false

      t.timestamps
    end

    add_index :profiles, [:entity_id, :name], unique: true
  end
end
