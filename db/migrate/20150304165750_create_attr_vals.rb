class CreateAttrVals < ActiveRecord::Migration
  def change
    create_table :attr_vals do |t|
      t.references  :attr,        index: true
      t.references  :profile,     index: true
      t.string      :domain,      limit: 500

      t.timestamps
    end
  end
end
