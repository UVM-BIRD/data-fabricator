class CreateConditions < ActiveRecord::Migration
  def change
    create_table :conditions do |t|
      t.references  :attr_val,    index: true
      t.references  :attr,        index: true
      t.string      :op,          length: 10
      t.string      :value,       length: 200
      t.string      :join_using,  length: 3

      t.timestamps
    end
  end
end
