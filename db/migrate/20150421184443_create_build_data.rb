class CreateBuildData < ActiveRecord::Migration
  def change
    create_table :build_data do |t|
      t.references  :user,      index: true
      t.references  :data_set,  index: true
      t.string      :thread_id
      t.string      :status
      t.string      :message
      t.string      :file

      t.timestamps
    end

    add_index :build_data, [:user_id, :data_set_id], unique: true
  end
end
