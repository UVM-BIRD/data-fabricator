# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150421184443) do

  create_table "attr_vals", force: true do |t|
    t.integer  "attr_id"
    t.integer  "profile_id"
    t.string   "domain",     limit: 500
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attr_vals", ["attr_id"], name: "index_attr_vals_on_attr_id", using: :btree
  add_index "attr_vals", ["profile_id"], name: "index_attr_vals_on_profile_id", using: :btree

  create_table "attrs", force: true do |t|
    t.integer  "entity_id"
    t.string   "name",       limit: 30
    t.string   "data_type",  limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "transient",             default: false
  end

  add_index "attrs", ["entity_id", "name"], name: "index_attrs_on_entity_id_and_name", unique: true, using: :btree
  add_index "attrs", ["entity_id"], name: "index_attrs_on_entity_id", using: :btree

  create_table "build_data", force: true do |t|
    t.integer  "user_id"
    t.integer  "data_set_id"
    t.string   "thread_id"
    t.string   "status"
    t.string   "message"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "build_data", ["data_set_id"], name: "index_build_data_on_data_set_id", using: :btree
  add_index "build_data", ["user_id", "data_set_id"], name: "index_build_data_on_user_id_and_data_set_id", unique: true, using: :btree
  add_index "build_data", ["user_id"], name: "index_build_data_on_user_id", using: :btree

  create_table "conditions", force: true do |t|
    t.integer  "attr_val_id"
    t.integer  "attr_id"
    t.string   "op"
    t.string   "value"
    t.string   "join_using"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "conditions", ["attr_id"], name: "index_conditions_on_attr_id", using: :btree
  add_index "conditions", ["attr_val_id"], name: "index_conditions_on_attr_val_id", using: :btree

  create_table "data_sets", force: true do |t|
    t.integer  "user_id"
    t.string   "name",       limit: 50
    t.string   "desc",       limit: 500
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "data_sets", ["user_id", "name"], name: "index_data_sets_on_user_id_and_name", unique: true, using: :btree
  add_index "data_sets", ["user_id"], name: "index_data_sets_on_user_id", using: :btree

  create_table "entities", force: true do |t|
    t.integer  "data_set_id"
    t.integer  "parent_entity_id"
    t.string   "name",             limit: 30
    t.string   "desc",             limit: 500
    t.integer  "min_recs",                     default: 1
    t.integer  "max_recs",                     default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "entities", ["data_set_id", "name"], name: "index_entities_on_data_set_id_and_name", unique: true, using: :btree
  add_index "entities", ["data_set_id"], name: "index_entities_on_data_set_id", using: :btree
  add_index "entities", ["parent_entity_id"], name: "index_entities_on_parent_entity_id", using: :btree

  create_table "profiles", force: true do |t|
    t.integer  "entity_id"
    t.string   "name",       limit: 50
    t.string   "desc",       limit: 500
    t.integer  "proportion",             default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "required",               default: false
  end

  add_index "profiles", ["entity_id", "name"], name: "index_profiles_on_entity_id_and_name", unique: true, using: :btree
  add_index "profiles", ["entity_id"], name: "index_profiles_on_entity_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
