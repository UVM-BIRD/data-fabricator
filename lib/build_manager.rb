# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'singleton'
require 'logging'

class BuildManager
  include Singleton, Logging

  def initialize
    log.info 'initializing build status registry'
    @registry = {}
  end

  def register(user, data_set, records_to_create)
    @registry[user.id] = {} unless @registry[user.id]

    hsh = { current: 0, total: records_to_create }
    @registry[user.id][data_set.id] = hsh

    hsh
  end

  def get_build_info(user, data_set)
    @registry[user.id] ?
        @registry[user.id][data_set.id] :
        nil
  end

  def deregister(user, data_set)
    @registry[user.id].delete(data_set.id) if @registry[user.id]
  end

  def cancel_build_for(user, data_set)
    cp = get_build_info user, data_set
    cp[:canceled] = true if cp
  end
end