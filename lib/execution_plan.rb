# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'build_manager'
require 'logging'

class ExecutionPlan
  include Logging

  def initialize(user, data_set)
    @user = user
    @data_set = data_set
    @entities_map = {}
    @plan = {}
    @totals = {}

    populate_maps

    # create an execution plan skeleton that represents each individual entity to be created.
    build_plan_skeleton

    # now that we know exactly what and how many of each entity we're going to create, tally up
    # the total counts of each entity type so we can figure out how many of them need to use
    # profile specs instead of default specs
    populate_totals

    apply_profiles_to_plan
  end

  def execute(dir)
    register_progress

    files = {}

    begin
      files[:sql] = File.new("#{dir}/createdb.sql", 'w')
      @data_set.entities.each do |entity|
        files[entity.id] = File.new("#{dir}/#{entity.name}.csv", 'w')
      end

      files[:sql].write @data_set.create_database_stmt

      ids = {}
      ordered_attrs_for_export = {}

      options = { user: @user, totals: @totals }

      # setup
      traverse_entity_tree do |entity|
        files[:sql].write entity.create_table_stmt(options)
        files[entity.id].write entity.csv_headers
        ids[entity.name.to_sym] = 1
        ordered_attrs_for_export[entity.name.to_sym] = entity.ordered_attrs_for_export
      end

      # build records
      traverse_plan_for_build do |entity_key, profile_id, parent|
        raise CanceledException if @progress[:canceled]

        entity = @entities_map[entity_key]

        rec = { id: ids[entity_key] }
        rec.merge! entity.fabricate_record(profile_id, parent, options)
        ids[entity_key] += 1

        files[:sql].write insert_stmt(entity.name, rec, ordered_attrs_for_export[entity_key])
        files[entity.id].write csv_record(rec, ordered_attrs_for_export[entity_key])

        @progress[:current] += 1

        rec
      end

    ensure
      deregister_progress

      files.values.each do |file|
        file.close
      end
    end
  end

  private
    def register_progress
      grand_total = 0
      @totals.each_value do |hsh|
        grand_total += hsh[:grand_total]
      end
      @progress = BuildManager.instance.register(@user, @data_set, grand_total)
    end

    def deregister_progress
      BuildManager.instance.deregister(@user, @data_set)
    end

    def populate_maps
      @data_set.entities.each do |entity|
        @entities_map[entity.name.to_sym] = entity
      end
    end

    def top_level_entities
      @data_set.entities.where(parent_entity_id: nil)
    end

    def apply_profiles_to_plan
      allocation = build_profile_allocation

      each_plan_entity do |entity_key, hsh|
        if allocation[entity_key]
          data = allocation[entity_key]

          current_index = data[:current_index]

          profile_id = nil
          data[:profiles].each_pair do |k, indexes|
            if indexes.include? current_index
              profile_id = k
              break
            end
          end

          hsh[:profile_id] = profile_id if profile_id

          data[:current_index] += 1
        end
      end
    end

    def build_profile_allocation
      allocation = {}

      @totals.each_pair do |entity_key, hsh|
        if hsh[:profiles]
          allocation[entity_key] = { current_index: 0, profiles: {} } unless allocation[entity_key]

          index_pool = (0...hsh[:grand_total]).to_a

          hsh[:profiles].each_pair do |profile_id, total|
            arr = []
            total.times do
              arr << index_pool.delete_at(rand(index_pool.size))
            end
            allocation[entity_key][:profiles][profile_id] = arr
          end
        end
      end

      allocation
    end

    def traverse_plan_for_build(plan = nil, parent = nil, &block)
      raise 'block required' unless block_given?

      plan = @plan if plan.nil?

      plan.each_pair do |entity_key, arr|
        arr.each do |hsh|
          if hsh[:children]
            rec = block.yield(entity_key, hsh[:profile_id], parent)

            new_parent = { entity_key: entity_key, rec: rec }
            new_parent[:parent] = parent if parent

            traverse_plan_for_build(hsh[:children], new_parent, &block)

          else
            block.yield(entity_key, hsh[:profile_id], parent)
          end
        end
      end
    end

    def each_plan_entity(plan = nil, &block)
      raise 'block required' unless block_given?

      plan = @plan if plan.nil?

      plan.each_pair do |entity_key, arr|
        arr.each do |hsh|
          block.yield(entity_key, hsh)
          each_plan_entity(hsh[:children], &block) if hsh[:children]
        end
      end
    end

    def traverse_entity_tree(entity = nil, &block)
      raise 'block required' unless block_given?

      if entity
        yield entity

        entity.children.each do |child|
          traverse_entity_tree child, &block
        end

      else
        top_level_entities.each do |top_level_entity|
          traverse_entity_tree top_level_entity, &block
        end
      end
    end

    def populate_totals(entity_key = nil, plan = nil)
      if entity_key && plan
        @totals[entity_key] = { grand_total: 0 } unless @totals[entity_key]
        @totals[entity_key][:grand_total] += plan.size

        plan.each do |rec|
          if rec[:children]
            rec[:children].each_pair do |child_entity_key, child_plan|
              populate_totals child_entity_key, child_plan
            end
          end
        end

      else
        @plan.each_pair do |ek, ep|
          populate_totals ek, ep
        end

        set_profile_totals
      end
    end

    def set_profile_totals(entity = nil)
      if entity
        totals = @totals[entity.name.to_sym]

        if entity.profiles.any?
          required_profiles_count = entity.profiles.where(required: true).count
          totals[:grand_total] = required_profiles_count if required_profiles_count > totals[:grand_total]

          totals[:profiles] = {} unless totals[:profiles]

          profiles_grand_total = 0
          entity.profiles.each do |profile|
            profile_total = (profile.proportion * totals[:grand_total]) / 100
            profile_total = 1 if profile_total == 0 && profile.required?

            if profile_total > 0
              profiles_grand_total += profile_total
              totals[:profiles][profile.id] = profile_total
            end
          end

          if profiles_grand_total > totals[:grand_total]
            raise "profiles total (#{profiles_grand_total}) > grand total for entity (#{entity.name}, #{totals[:grand_total]})"
          end
        end

      else
        @data_set.entities.each do |ent|
          set_profile_totals ent
        end
      end
    end

    def build_plan_skeleton(entity = nil)
      if entity
        recs_to_create = recs_to_create_for_entity(entity)
        has_children = has_children?(entity)
        plan = []

        recs_to_create.times do
          rec = {}
          if has_children
            children_plans = {}
            entity.children.each do |child_entity|
              children_plans[child_entity.name.to_sym] = build_plan_skeleton(child_entity)
            end
            rec[:children] = children_plans
          end
          plan << rec
        end

        plan

      else
        top_level_entities.each do |ent|
          @plan[ent.name.to_sym] = build_plan_skeleton(ent)
        end
      end
    end

    # cache this information because otherwise the call to entity.children.any? gets called waaaaaay too much and even
    # though it's a simple call, it's extra SQL that doesn't need to be executed a zillion times
    def has_children?(entity)
      unless @entity_has_children
        @entity_has_children = {}
        @data_set.entities.each do |ent|
          @entity_has_children[ent.id] = ent.children.any?
        end
      end
      @entity_has_children[entity.id]
    end

    def recs_to_create_for_entity(entity)
      recs_to_create = entity.min_recs
      recs_to_create += rand(entity.max_recs - entity.min_recs + 1) if entity.max_recs > entity.min_recs
      recs_to_create
    end

    def insert_stmt(table_name, rec, export_attrs)
      hsh = {}
      export_attrs.each do |attr|
        val = rec[attr]
        hsh[attr] = case val
                      when NilClass then    'null'
                      when TrueClass then   1
                      when FalseClass then  0
                      when String then      "\"#{sql_escape(val)}\""
                      when Fixnum then      val
                      when Float then       val
                      when DateTime then    "\"#{val.strftime('%Y-%m-%d %H:%M:%S')}\""
                      when Date then        "\"#{val.strftime('%Y-%m-%d')}\""
                      else                  raise "unhandled class '#{val.class}'"
                    end
      end

      "insert into #{table_name} (#{hsh.keys.join(', ')}) values(#{hsh.values.join(', ')});\n"
    end

    def csv_record(rec, export_attrs)
      arr = []
      export_attrs.each do |attr|
        val = rec[attr]
        v = case val
              when String then    csv_wrap_quotes_if_needed(csv_escape(val))
              when DateTime then  val.strftime('%Y-%m-%d %H:%M:%S')
              when Date then      val.strftime('%Y-%m-%d')
              else                val
            end
        arr << v
      end

      "#{arr.join(',')}\n"    # important!  no whitespace around comma delimiter!
    end

    def sql_escape(s)
      s.gsub('\\', '\\\\\\\\')
          .gsub('"', '\\"')
    end

    def csv_wrap_quotes_if_needed(s)
      s.include?(',') ? "\"#{s}\"" : s
    end

    def csv_escape(s)
      s.gsub('"', '""')
    end
end