# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'java'
require 'singleton'
require 'logging'
require 'securerandom'

class ExecutorPool
  include Singleton, Logging
  java_import java.util.concurrent.Executors

  def initialize
    log.info 'initializing new cached thread pool'
    @executor = Executors.newCachedThreadPool
    @futures = {}
  end

  def execute(params = {})
    raise 'block required' unless block_given?

    thread_id = params.key?(:thread_id) ?
        params[:thread_id] :
        SecureRandom.hex(5)

    # require sequential execution in test environment!
    if Rails.env == 'test'
      begin
        log.info "begin faux-thread #{thread_id}"
        yield thread_id

      rescue Exception => e
        log.error "caught #{e.class.name} executing faux-thread #{thread_id} : #{e.message}"
        log.debug e.backtrace.join("\n")
        raise e

      ensure
        log.info "end faux-thread #{thread_id}"
      end

    else
      future = @executor.submit do
        begin
          log.info "begin thread #{thread_id}"
          yield thread_id

        rescue Exception => e
          log.error "caught #{e.class.name} executing thread #{thread_id} : #{e.message}"
          log.debug e.backtrace.join("\n")
          raise e

        ensure
          log.info "end thread #{thread_id}"
          deregister_thread_future thread_id
        end
      end

      register_thread_future thread_id, future
    end

    thread_id
  end

  def shutdown
    log.info 'shutting down'
    cancel_all_threads
    @executor.shutdownNow
  end

  def cancel_all_threads
    @futures.each_key do |thread_id|
      cancel thread_id
    end
  end

  def cancel(thread_id)
    if @futures.key? thread_id
      if @futures[thread_id].cancel(true)
        log.info "*** thread #{thread_id} cancelled ***"
        deregister_thread_future thread_id
        return true

      else
        log.warn "thread #{thread_id} could not be cancelled.  perhaps it already finished?"
      end

    else
      log.error "invalid thread : #{thread_id}"
    end

    false
  end

  private

  def register_thread_future(thread_id, future)
    log.info "registering future for thread #{thread_id}"
    @futures[thread_id] = future
  end

  def deregister_thread_future(thread_id)
    if @futures && @futures.key?(thread_id)
      log.info "deregistering future for thread #{thread_id}"
      @futures.delete thread_id

    else
      log.error "invalid thread #{thread_id}"
    end
  end
end