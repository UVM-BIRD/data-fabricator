# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'java'
require 'logging'
require 'singleton'
require 'invalid_expr_exception'

class ExprEvaluator
  include Singleton
  include Logging

  def initialize
    @evaluator = Java::edu.uvm.ccts.dataFabricator.expr.ExprEvaluator.new
  end

  def validate(expr)
    begin
      @evaluator.validate_parse(expr)
      true

    rescue Exception => e
      log.error "'#{expr}' failed to validate as expression : #{e.message}"
      raise InvalidExprException.new(e)
    end
  end

  def evaluate(expr)
    begin
      @evaluator.evaluate(expr)

    rescue Exception => e
      log.error "couldn't evaluate expression #{expr}' : #{e.message}"
      nil
    end
  end
end