# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'java'

class Log4jAdapter < Logger
  include ActiveSupport::Logger::Severity

  LEVEL_MAP = {
      DEBUG => org.apache.logging.log4j.Level::DEBUG,
      INFO => org.apache.logging.log4j.Level::INFO,
      WARN => org.apache.logging.log4j.Level::WARN,
      ERROR => org.apache.logging.log4j.Level::ERROR,
      FATAL => org.apache.logging.log4j.Level::FATAL
  } unless defined? LEVEL_MAP

  LEVEL_MAP_INVERSE = LEVEL_MAP.invert unless defined? LEVEL_MAP_INVERSE

  def initialize(owner)
    case owner
      when Class  then logger_name = owner.name
      when String then logger_name = owner
      when Object then logger_name = owner.class.name
      else
        raise 'Unrecognized log4j logger'
    end

    @logger = org.apache.logging.log4j.LogManager.getLogger(logger_name)
  end

  def add(severity, message = nil, progname = nil, &block)
    raise 'Invalid log level' unless LEVEL_MAP[severity.to_i]

    message = (message || (block && block.call) || progname).to_s
    @logger.log(LEVEL_MAP[severity.to_i], message)
  end

  def level
    LEVEL_MAP_INVERSE[@logger.getLevel]
  end

  def enabled_for?(severity)
    raise 'Invalid log level' unless LEVEL_MAP[severity.to_i]
    @logger.isEnabledFor(LEVEL_MAP[severity.to_i])
  end

  #Lifted from BufferedLogger
  for severity in ActiveSupport::Logger::Severity.constants
    class_eval <<-EOT, __FILE__, __LINE__
      def #{severity.downcase}(message = nil, progname = nil, &block)  # def debug(message = nil, progname = nil, &block)
        add(#{severity}, message, progname, &block)                    #   add(DEBUG, message, progname, &block)
      end                                                              # end
                                                                       #
      def #{severity.downcase}?                                        # def debug?
        enabled_for?(#{severity})                                      #   DEBUG >= @level
      end                                                              # end
    EOT
  end
end