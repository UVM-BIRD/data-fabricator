# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

module Logging
  def self.included(base)
    base.extend ClassMethods
  end

  private

  def log
    @log ||= Log4jAdapter.new self.class.name
  end

  def log_error(e, action = nil)
    log.error "caught #{e.class.name} #{action || ''} - #{e}\n#{e.backtrace.join("\n")}"
  end

  module ClassMethods
    private

    def log
      @log ||= Log4jAdapter.new self
    end
  end
end