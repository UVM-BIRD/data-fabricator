# Copyright 2015 The University of Vermont and State Agricultural
# College.  All rights reserved.
#
# Written by Matthew B. Storer <matthewbstorer@gmail.com>
#
# This file is part of Data Fabricator.
#
# Data Fabricator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Data Fabricator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

require 'zip'

class ZipUtil
  include Logging

  def self.create_zip(src, target = nil)
    input_filenames = []
    if File.directory? src
      dir = src
      ignore_dirs = %w(. ..)
      Dir.foreach(src) do |f|
        input_filenames << f unless ignore_dirs.include?(f)
      end

    elsif File.file? src
      dir = File.dirname src
      input_filenames << src

    else
      raise "#{src} does not exist"
    end

    if target.nil?
      target = File.join(dir, 'archive.zip')

    elsif target == File.basename(target)
      target = File.join(dir, target)
    end

    Zip::File.open(target, Zip::File::CREATE) do |zipfile|
      log.info("*** creating zip file '#{target}'")
      input_filenames.each do |filename|
        log.info " -> deflating #{filename}"
        zipfile.add(filename, File.join(dir, filename))
      end
    end

    target
  end
end