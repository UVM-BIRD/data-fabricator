// Copyright 2015 The University of Vermont and State Agricultural
// College.  All rights reserved.
//
// Written by Matthew B. Storer <matthewbstorer@gmail.com>
//
// This file is part of Data Fabricator.
//
// Data Fabricator is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Fabricator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.

grammar Expr;

init : expr EOF ;

expr : '(' expr ')'             # parens
     | expr Pow expr            # raiseToPower
     | expr Mul expr            # multiply
     | expr Div expr            # divide
     | expr Plus expr           # add
     | expr Minus expr          # subtract
     | Number                   # number
     ;

Pow : '^' ;
Mul : '*' ;
Div : '/' ;
Plus : '+' ;
Minus : '-' ;

Number : FLOAT
       | Int
       ;

Int : ('-'|'+')? DIGIT
    | ('-'|'+')? NON_ZERO_DIGIT DIGIT+
    ;

fragment FLOAT : Int* '.' DIGIT+ (E Int)?
               | Int '.' DIGIT* (E Int)?
               | Int E Int
               ;
fragment DIGIT : [0-9] ;
fragment NON_ZERO_DIGIT : [1-9] ;
fragment E : [Ee] ;

WS : [ \t\r\n]+ -> skip ;
