/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.dataFabricator.expr;

import edu.uvm.ccts.dataFabricator.expr.antlr.ExprLexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.LexerNoViableAltException;

/**
 * Created by mstorer on 4/29/15.
 */
public class BailExprLexer extends ExprLexer {
    public BailExprLexer(CharStream input) {
        super(input);
    }

    @Override
    public void recover(LexerNoViableAltException e) {
        String s = e.getInputStream().toString();
        String near = s.substring(Math.max(0, e.getStartIndex() - 5),
                Math.min(e.getStartIndex() + 5, s.length() - 1));

        String message = "failed to lex expression at index " + e.getStartIndex() + " near \"" + near + "\"";

        throw new RuntimeException(message, e);
    }
}
