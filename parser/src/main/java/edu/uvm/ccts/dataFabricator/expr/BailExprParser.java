/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.dataFabricator.expr;

import edu.uvm.ccts.dataFabricator.expr.antlr.ExprParser;
import edu.uvm.ccts.dataFabricator.model.CustomParserErrorHandler;
import org.antlr.v4.runtime.TokenStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mstorer on 4/29/15.
 */
public class BailExprParser extends ExprParser {
    private static final Log log = LogFactory.getLog(ExprParser.class);

    public BailExprParser(TokenStream input) {
        super(input);
        setErrorHandler(new CustomParserErrorHandler(log));
    }
}
