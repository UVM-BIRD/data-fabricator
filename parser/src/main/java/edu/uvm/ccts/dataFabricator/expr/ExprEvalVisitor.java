/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.dataFabricator.expr;

import edu.uvm.ccts.dataFabricator.expr.antlr.ExprBaseVisitor;
import edu.uvm.ccts.dataFabricator.expr.antlr.ExprParser;
import edu.uvm.ccts.dataFabricator.util.NumberUtil;
import org.antlr.v4.runtime.misc.NotNull;

/**
 * Created by mstorer on 4/29/15.
 */
public class ExprEvalVisitor extends ExprBaseVisitor<Number> {
    private Number value = null;

    public Number getValue() {
        return value;
    }

    @Override
    public Number visitInit(@NotNull ExprParser.InitContext ctx) {
        value = visit(ctx.expr());
        return super.visitInit(ctx);
    }

    @Override
    public Number visitParens(@NotNull ExprParser.ParensContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Number visitRaiseToPower(@NotNull ExprParser.RaiseToPowerContext ctx) {
        Number left = visit(ctx.expr(0));
        Number right = visit(ctx.expr(1));
        return NumberUtil.raiseToPower(left, right);
    }

    @Override
    public Number visitMultiply(@NotNull ExprParser.MultiplyContext ctx) {
        Number left = visit(ctx.expr(0));
        Number right = visit(ctx.expr(1));
        return NumberUtil.multiply(left, right);
    }

    @Override
    public Number visitDivide(@NotNull ExprParser.DivideContext ctx) {
        Number left = visit(ctx.expr(0));
        Number right = visit(ctx.expr(1));
        return NumberUtil.divide(left, right);
    }

    @Override
    public Number visitAdd(@NotNull ExprParser.AddContext ctx) {
        Number left = visit(ctx.expr(0));
        Number right = visit(ctx.expr(1));
        return NumberUtil.add(left, right);
    }

    @Override
    public Number visitSubtract(@NotNull ExprParser.SubtractContext ctx) {
        Number left = visit(ctx.expr(0));
        Number right = visit(ctx.expr(1));
        return NumberUtil.subtract(left, right);
    }

    @Override
    public Number visitNumber(@NotNull ExprParser.NumberContext ctx) {
        return NumberUtil.parseNumber(ctx.Number().getText());
    }
}
