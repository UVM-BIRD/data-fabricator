/*
 * Copyright 2015 The University of Vermont and State Agricultural
 * College.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.dataFabricator.expr;

import edu.uvm.ccts.dataFabricator.expr.antlr.ExprBaseVisitor;
import edu.uvm.ccts.dataFabricator.expr.antlr.ExprLexer;
import edu.uvm.ccts.dataFabricator.expr.antlr.ExprParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mstorer on 4/29/15.
 */
public class ExprEvaluator {
    public void validateParse(String expr) throws IOException {
        new ExprBaseVisitor().visit(buildParseTree(expr));
    }

    public Number evaluate(String expr) throws IOException {
        ParseTree tree = buildParseTree(expr);
        ExprEvalVisitor visitor = new ExprEvalVisitor();
        visitor.visit(tree);
        return visitor.getValue();
    }

    private ParseTree buildParseTree(String expr) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(expr.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        ExprLexer lexer = new BailExprLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ExprParser parser = new BailExprParser(tokens);
        return parser.init();
    }
}
