// Generated from Expr.g4 by ANTLR 4.1
package edu.uvm.ccts.dataFabricator.expr.antlr;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ExprLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__1=1, T__0=2, Pow=3, Mul=4, Div=5, Plus=6, Minus=7, Number=8, Int=9, 
		WS=10;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"')'", "'('", "'^'", "'*'", "'/'", "'+'", "'-'", "Number", "Int", "WS"
	};
	public static final String[] ruleNames = {
		"T__1", "T__0", "Pow", "Mul", "Div", "Plus", "Minus", "Number", "Int", 
		"FLOAT", "DIGIT", "NON_ZERO_DIGIT", "E", "WS"
	};


	public ExprLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Expr.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 13: WS_action((RuleContext)_localctx, actionIndex); break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0: skip();  break;
		}
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\2\fq\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3"+
		"\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\5\t\60\n\t\3\n\5\n\63\n\n\3\n\3\n\5"+
		"\n\67\n\n\3\n\3\n\6\n;\n\n\r\n\16\n<\5\n?\n\n\3\13\7\13B\n\13\f\13\16"+
		"\13E\13\13\3\13\3\13\6\13I\n\13\r\13\16\13J\3\13\3\13\3\13\5\13P\n\13"+
		"\3\13\3\13\3\13\7\13U\n\13\f\13\16\13X\13\13\3\13\3\13\3\13\5\13]\n\13"+
		"\3\13\3\13\3\13\3\13\5\13c\n\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\6\17l\n"+
		"\17\r\17\16\17m\3\17\3\17\2\20\3\3\1\5\4\1\7\5\1\t\6\1\13\7\1\r\b\1\17"+
		"\t\1\21\n\1\23\13\1\25\2\1\27\2\1\31\2\1\33\2\1\35\f\2\3\2\7\4\2--//\3"+
		"\2\62;\3\2\63;\4\2GGgg\5\2\13\f\17\17\"\"y\2\3\3\2\2\2\2\5\3\2\2\2\2\7"+
		"\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2"+
		"\2\2\23\3\2\2\2\2\35\3\2\2\2\3\37\3\2\2\2\5!\3\2\2\2\7#\3\2\2\2\t%\3\2"+
		"\2\2\13\'\3\2\2\2\r)\3\2\2\2\17+\3\2\2\2\21/\3\2\2\2\23>\3\2\2\2\25b\3"+
		"\2\2\2\27d\3\2\2\2\31f\3\2\2\2\33h\3\2\2\2\35k\3\2\2\2\37 \7+\2\2 \4\3"+
		"\2\2\2!\"\7*\2\2\"\6\3\2\2\2#$\7`\2\2$\b\3\2\2\2%&\7,\2\2&\n\3\2\2\2\'"+
		"(\7\61\2\2(\f\3\2\2\2)*\7-\2\2*\16\3\2\2\2+,\7/\2\2,\20\3\2\2\2-\60\5"+
		"\25\13\2.\60\5\23\n\2/-\3\2\2\2/.\3\2\2\2\60\22\3\2\2\2\61\63\t\2\2\2"+
		"\62\61\3\2\2\2\62\63\3\2\2\2\63\64\3\2\2\2\64?\5\27\f\2\65\67\t\2\2\2"+
		"\66\65\3\2\2\2\66\67\3\2\2\2\678\3\2\2\28:\5\31\r\29;\5\27\f\2:9\3\2\2"+
		"\2;<\3\2\2\2<:\3\2\2\2<=\3\2\2\2=?\3\2\2\2>\62\3\2\2\2>\66\3\2\2\2?\24"+
		"\3\2\2\2@B\5\23\n\2A@\3\2\2\2BE\3\2\2\2CA\3\2\2\2CD\3\2\2\2DF\3\2\2\2"+
		"EC\3\2\2\2FH\7\60\2\2GI\5\27\f\2HG\3\2\2\2IJ\3\2\2\2JH\3\2\2\2JK\3\2\2"+
		"\2KO\3\2\2\2LM\5\33\16\2MN\5\23\n\2NP\3\2\2\2OL\3\2\2\2OP\3\2\2\2Pc\3"+
		"\2\2\2QR\5\23\n\2RV\7\60\2\2SU\5\27\f\2TS\3\2\2\2UX\3\2\2\2VT\3\2\2\2"+
		"VW\3\2\2\2W\\\3\2\2\2XV\3\2\2\2YZ\5\33\16\2Z[\5\23\n\2[]\3\2\2\2\\Y\3"+
		"\2\2\2\\]\3\2\2\2]c\3\2\2\2^_\5\23\n\2_`\5\33\16\2`a\5\23\n\2ac\3\2\2"+
		"\2bC\3\2\2\2bQ\3\2\2\2b^\3\2\2\2c\26\3\2\2\2de\t\3\2\2e\30\3\2\2\2fg\t"+
		"\4\2\2g\32\3\2\2\2hi\t\5\2\2i\34\3\2\2\2jl\t\6\2\2kj\3\2\2\2lm\3\2\2\2"+
		"mk\3\2\2\2mn\3\2\2\2no\3\2\2\2op\b\17\2\2p\36\3\2\2\2\17\2/\62\66<>CJ"+
		"OV\\bm";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}