// Generated from Expr.g4 by ANTLR 4.1
package edu.uvm.ccts.dataFabricator.expr.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ExprParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ExprVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ExprParser#subtract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtract(@NotNull ExprParser.SubtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(@NotNull ExprParser.InitContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#divide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivide(@NotNull ExprParser.DivideContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#multiply}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(@NotNull ExprParser.MultiplyContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#raiseToPower}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRaiseToPower(@NotNull ExprParser.RaiseToPowerContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(@NotNull ExprParser.NumberContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#parens}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParens(@NotNull ExprParser.ParensContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(@NotNull ExprParser.AddContext ctx);
}