/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Data Fabricator.
 *
 * Data Fabricator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Data Fabricator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Data Fabricator.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.dataFabricator.util;

/**
 * Created by mstorer on 4/29/15.
 */
public class NumberUtil {
    public static Number parseNumber(String s) {
        boolean isFloat = s.indexOf('.') >= 0 || s.indexOf('e') >= 0 || s.indexOf('E') >= 0;
        if (isFloat) {
            return Double.parseDouble(s);

        } else {
            return Long.parseLong(s);
        }
    }

    public static boolean isWholeNumber(Number n) {
        return n != null && (Math.abs(n.doubleValue() - n.longValue()) <= 0.0000001);
    }

    public static Number add(Number a, Number b) {
        Number n;
        if (isWholeNumber(a)) {
            n = a.longValue() + (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        } else {
            n = a.doubleValue() + (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        }
        return simplify(n);
    }

    public static Number subtract(Number a, Number b) {
        Number n;
        if (isWholeNumber(a)) {
            n = a.longValue() - (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        } else {
            n = a.doubleValue() - (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        }
        return simplify(n);
    }

    public static Number multiply(Number a, Number b) {
        Number n;
        if (isWholeNumber(a)) {
            n = a.longValue() * (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        } else {
            n = a.doubleValue() * (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        }
        return simplify(n);
    }

    public static Number divide(Number a, Number b) {
        Number n;
        if (isWholeNumber(a)) {
            n = a.longValue() / (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        } else {
            n = a.doubleValue() / (isWholeNumber(b) ? b.longValue() : b.doubleValue());
        }
        return simplify(n);
    }

    public static Number raiseToPower(Number a, Number b) {
        Number n = Math.pow(a.doubleValue(), b.doubleValue());
        return simplify(n);
    }

    private static Number simplify(Number n) {
        // weird ... using ternary operator always generates a double, never a long -
        //  return isWholeNumber(n) ? n.longValue() : n.doubleValue();
        // very strange.  the following works though -

        if (isWholeNumber(n)) {
            return n.longValue();
        } else {
            return n.doubleValue();
        }
    }
}
