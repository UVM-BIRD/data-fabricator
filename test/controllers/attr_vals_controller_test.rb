require 'test_helper'

class AttrValsControllerTest < ActionController::TestCase
  setup do
    @attr_val = attr_vals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attr_vals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attr_val" do
    assert_difference('AttrVal.count') do
      post :create, attr_val: { attr_id: @attr_val.attr_id, condition: @attr_val.condition, domain: @attr_val.domain }
    end

    assert_redirected_to attr_val_path(assigns(:attr_val))
  end

  test "should show attr_val" do
    get :show, id: @attr_val
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attr_val
    assert_response :success
  end

  test "should update attr_val" do
    patch :update, id: @attr_val, attr_val: { attr_id: @attr_val.attr_id, condition: @attr_val.condition, domain: @attr_val.domain }
    assert_redirected_to attr_val_path(assigns(:attr_val))
  end

  test "should destroy attr_val" do
    assert_difference('AttrVal.count', -1) do
      delete :destroy, id: @attr_val
    end

    assert_redirected_to attr_vals_path
  end
end
